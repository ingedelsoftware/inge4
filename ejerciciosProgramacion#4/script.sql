-- Database: login

-- DROP DATABASE IF EXISTS login;

CREATE DATABASE login;
CREATE SEQUENCE users_id_seq;
CREATE TABLE users (
       id integer NOT NULL DEFAULT nextval('users_id_seq'::regclass),
    nombre character varying(50) COLLATE pg_catalog."default",
    apellido character varying(50) COLLATE pg_catalog."default",
    nombre_usuario character varying(50) COLLATE pg_catalog."default" NOT NULL,
    contrasena character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT users_pkey PRIMARY KEY (id)
);