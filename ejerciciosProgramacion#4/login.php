<?php
session_start();

try {
    $pdo = new PDO('pgsql:host=localhost;port=5432;dbname=login;user=postgres;password=postgres');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Error de conexión: " . $e->getMessage();
    exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $usuario = $_POST['nombre_usuario'];
    $contrasena = $_POST['contrasena'];

    $sql = "SELECT * FROM users WHERE nombre_usuario = :usuario";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':usuario', $usuario);
    $stmt->execute();
    $usuarioEncontrado = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($usuarioEncontrado && password_verify($contrasena, $usuarioEncontrado['contrasena'])) {
        $_SESSION['nombre'] = $usuarioEncontrado['nombre'];
        $_SESSION['apellido'] = $usuarioEncontrado['apellido'];
        
        
        header('Location: welcome.php');
    } else {
        echo '<p style="color: red;">Usuario o contraseña incorrectos.</p>';
    }
}
?>