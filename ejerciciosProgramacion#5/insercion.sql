-- insercion empresa

INSERT INTO public."Empresa"(
	id_empresa, nombre)
	VALUES 
	(1,'The Coca-Cola Company'),(2,'PEPSI-CO'),(3,'Companhia Antarctica Paulista'),
	(4,'Bebidas del Paraguay'),(5,'SELTZ'),(6,'Keurig Dr Pepper');
-- insercion marca
INSERT INTO public."Marca"(
	 id_marca, id_empresa, nombre)
	VALUES
	(1, 1, 'Coca-Cola'),(2, 2, 'Pepsi'),(3, 3, 'Guarana Antartica'),
	(4, 4, 'Pulp'),(5, 5, 'Vita'),(6,6, 'Dr Pepper');
-- insercion categoria
INSERT INTO public."Categoria"(
	 id_categoria,nombre)
	VALUES (1,'Normal'),(2,'Light'),(3,'Cero azucar');
-- insercion producto
INSERT INTO public."Producto"(
	id_producto,id_marca, id_categoria, nombre,precio)
	VALUES
	(1, 1, 1, 'Coca-Cola 2 lts',13000),(2, 2, 3, 'Pepsi Black 2 lts',9200),(3, 3, 3, 'Guarana Antartica Zero 500ml',5350),
	(4, 4, 3, 'Pulp Light 500ml',5350),(5, 5, 1, 'Vita cola 1.5lts',8200),(6, 6, 1,'Dr. Pepper 500ml',10000);