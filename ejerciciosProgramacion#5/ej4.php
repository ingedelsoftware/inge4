<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    
            table, th, td {
                border: 1px solid black;
            }
    </style>
</head>
<body>
    <table>
        <thead>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Marca</th>
            <th>Empresa</th>
            <th>Categoria</th>
        </thead>
        <tbody>
        <?php
            $query = '
                SELECT
                    p.nombre AS Nombre_Producto,
                    p.precio as Precio_producto,
                    m.nombre AS Nombre_Marca,
                    e.nombre AS Nombre_Empresa,
                    c.nombre AS Nombre_Categoria
                FROM
                    "Producto" p
                JOIN
                    "Marca" m ON p.id_marca = m.id_marca
                JOIN
                    "Categoria" c ON p.id_categoria = c.id_categoria
                JOIN
                    "Empresa" e ON m.id_empresa = e.id_empresa;
            ';
            $connection = ['user'=>'postgres', 'password'=>'admin', 'dbname'=>'ejercicio1', 'host'=>'localhost', 'port'=>'5432'];
            
            try{
                $pdo = new PDO("pgsql:host={$connection['host']} port={$connection['port']} dbname={$connection['dbname']} user={$connection['user']} password={$connection['password']}");
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                echo "Error de conexión: " . $e->getMessage();
                exit;
            }
            $stmt = $pdo->prepare($query);
            $stmt->execute();

            $listado = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            foreach ($listado as $row) {
                echo '<tr>';
                foreach($row as $key => $value){
                    echo '<td>';
                    echo $value;
                    echo '</td>';
                }
                echo '</tr>';

            }
        ?>
        </tbody>
    </table>
</body>
</html>