<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Juegos Gratuitos</title>
    <script
        src="https://code.jquery.com/jquery-3.7.1.js"
        integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- poner background dark-mode -->
    <style>
        .bg-dark {
            background-color: #272b30!important;
            color: white;
        }
    </style>
</head>
<body class="bg-dark">
    <h1>
        Plataformas
    </h1>
    <div id="list" class="row">
    </div>  
    
</body>
<script>
    function getGames(platform = '', categories = ''){
        const data = null;

        const xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.addEventListener('readystatechange', function () {
            if (this.readyState === this.DONE) {
                if (xhr.status >= 200 && xhr.status < 300) {
                    var games = JSON.parse(xhr.responseText)
                    var categories = createCategories(games);
                    showGames(games,categories);
                }
            }
        });

        xhr.open('GET', 'https://free-to-play-games-database.p.rapidapi.com/api/games');
        xhr.setRequestHeader('X-RapidAPI-Key', '430ee0ddb4mshe443a4940c6ac9fp1112a2jsn81b7df5f662b');
        xhr.setRequestHeader('X-RapidAPI-Host', 'free-to-play-games-database.p.rapidapi.com');

        xhr.send(data);
        
    }
    
    function createCategories(list){
        var arr =[];
        list.forEach(function(element){
            
            if (!arr.find(item => item === (element.platform))) {
                const platform = (element.platform);
                arr.push(platform); 
            }
        });
        arr.forEach(element => {
            console.log()
            $('#list').append("<div class='col-4' id='"+(element).split(' ').join('').split('(').join('').split(')').join('').split(',').join('')+"' ><h4>"+(element).split(' ').join('_').split('(').join('').split(')').join('').split(',').join('')+"</h4></div>");
        });
        return arr;
    }
    function showGames(list,arr){
        var html = '';
        for (let index = 0; index < list.length; index++) {
            const element = list[index];
            html += '<li>'+element.title+'</li>';
            const platform = (element.platform).split(' ').join('').split('(').join('').split(')').join('').split(',').join('')
            $('#'+platform).append('<li>'+element.title+'</li>');
        }
    }
    $(document).ready(function(){
        getGames();
        
    });

</script>
</html>