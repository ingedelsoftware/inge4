<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bromas Para Programadores</title>
    <script
        src="https://code.jquery.com/jquery-3.7.1.js"
        integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style>
        body{
            background-image:url('./descarga.jpg');
            background-repeat:repeat;
        }
    </style>
</head>
<body>
<div class="container mt-5">
        <div class="row">
            <!-- Left Side -->
            <div class="col-lg-6 bg-success" id="leftSide">
                <h1>Para un chiste en ingles presionar en esta area</h1>
            </div>

            <!-- Right Side -->
            <div class="col-lg-6 bg-danger" id="rightSide">
                <h1>Para un chiste en espa&ntilde;ol preisonar en esta area</h1>
            </div>
        </div>
    </div>
    <div class="container mt-5" id="jokeDiv" style="display:none">
        <div class="row  rounded-75 bg-info">
            <div class="col-lg-6 offset-lg-3 text-center">
                <p class="joke" id="joke"></p>
                <p class="setup" id="setup"></p>
                <p class="delivery" id="delivery"></p>
                <p class="error" id="error"></p>
            </div>
        </div>
    </div>
</body>
<script>
    $(document).ready(function () {
        $('#leftSide').on('click',function(){
            $('#jokeDiv').hide();
            $('#joke').empty();
            $('#setup').empty();
            $('#delivery').empty();
            $('#error').empty();

            getJoke('en');
        });
        $('#rightSide').on('click',function(){
            $('#jokeDiv').hide();
            $('#joke').empty();
            $('#setup').empty();
            $('#delivery').empty();
            $('#error').empty();

            getJoke('es');
        });

    });
    function getJoke(lang){
        var baseURL = "https://v2.jokeapi.dev";//url base dle endpoint a consultar
        var categories = ["Programming","pun"];//tipode de chistes que pedimos
        var params = [
            "blacklistFlags=nsfw",
            "lang="+lang
        ];//paramaetros extras

        var xhr = new XMLHttpRequest();
        xhr.open("GET", baseURL + "/joke/" + categories.join(",") + "?" + params.join("&"));

        xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && xhr.status < 300) // readyState 4 means request has finished + we only want to parse the joke if the request was successful (status code lower than 300)
        {
            $('#jokeDiv').show();
            var randomJoke = JSON.parse(xhr.responseText);

            if(randomJoke.type == "single")
            {
                // If type == "single", the joke only has the "joke" property
                $('#joke').html(randomJoke.joke);
                
            }
            else
            {
                // If type == "single", the joke only has the "joke" property
                $('#setup').html(randomJoke.setup);
                $('#delivery').html(randomJoke.delivery);
            }
        }
        else if(xhr.readyState == 4)
        {
            $('#error').html("Ocurrio un error mientras se solicitaba el chiste.Codigo de status" + xhr.status + "\nRepuesta del servidor: " + xhr.responseText);
        }
    };

    xhr.send();    
    }
    
</script>

</html>