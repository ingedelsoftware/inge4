<?php

try {
    $db = new PDO("pgsql:host=localhost port=5432 dbname=ejercicio4 user=postgres password=postgres sslmode=prefer connect_timeout=10");
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    // Preparar la consulta de inserción
    $sql = "INSERT INTO tabla (nombre, descripcion) VALUES (:nombre, :descripcion)";
    $stmt = $db->prepare($sql);

    // Generar e insertar 1000 registros aleatorios
    for ($i = 1; $i <= 1000; $i++) {
        $nombre = "Registro " . $i;
        $descripcion = "Descripción aleatoria " . rand(1, 1000);

        $stmt->bindParam(':nombre', $nombre, PDO::PARAM_STR);
        $stmt->bindParam(':descripcion', $descripcion, PDO::PARAM_STR);
        
        $stmt->execute();
    }
    
    echo "Se han insertado 1000 registros en la tabla 'tabla' de PostgreSQL.";

     // Consulta para seleccionar los registros
     $sql = "SELECT id, nombre, descripcion FROM tabla";
     $stmt = $db->prepare($sql);
     $stmt->execute();
 
     // Iniciar la tabla HTML
     echo '<table border="1">
           <tr>
             <th>ID</th>
             <th>Nombre</th>
             <th>Descripción</th>
           </tr>';
 
     // Recorrer y mostrar los registros en una tabla
     while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
         echo '<tr>';
         echo '<td>' . $row['id'] . '</td>';
         echo '<td>' . $row['nombre'] . '</td>';
         echo '<td>' . $row['descripcion'] . '</td>';
         echo '</tr>';
     }
 
     // Cerrar la tabla HTML
     echo '</table>';
 

} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>
