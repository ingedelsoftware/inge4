<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .green {
            color: green;
        }
        .red {
            color: red;
        }
    </style>
</head>
<body>
<!-- 4-Ejercicio 4:
Hacer un script PHP que haga lo siguiente:
• Declarar 3 variable con valores enteros aleatorios (se debe generar aleatoriamente estos
valores entre 50 y 900) – Se debe usar la función mt_srand y mt_rand.
• Ordenar de mayor a menor los valores de estas variables.
• Imprimir en pantalla la secuencia de números ordenadas, los números deben estar
separados por espacios en blanco.
• El número mayor debe estar en color verde y el número más pequeño debe estar en color
rojo. -->
<?php
    
    mt_srand(); 
    $num1 = mt_rand(50, 900);
    $num2 = mt_rand(50, 900);
    $num3 = mt_rand(50, 900);

    
    $numeros = array($num1, $num2, $num3);
    rsort($numeros);

    
    echo "<p class='green'>" . $numeros[0] . "</p>";
    echo "<p>" . $numeros[1] . "</p>";
    echo "<p class='red'>" . $numeros[2] . "</p>";
    ?>
</body>
</html>