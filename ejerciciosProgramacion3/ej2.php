<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<!-- Hacer un script PHP que imprime la siguiente información:
• Versión de PHP utilizada.
• El id de la versión de PHP.
• El valor máximo soportado para enteros para esa versión.
• Tamaño máximo del nombre de un archivo.
• Versión del Sistema Operativo.
• Símbolo correcto de 'Fin De Línea' para la plataforma en uso.
• El include path por defecto.
Observación: Ver las constantes predefinidas del núcleo de PHP -->
<?php

echo "Versión de PHP utilizada: " . PHP_VERSION . "<br>";

echo "ID de la versión de PHP: " . PHP_VERSION_ID . "<br>";

echo "Valor máximo soportado para enteros: " . PHP_INT_MAX . "<br>";

echo "Tamaño máximo del nombre de un archivo: " . PHP_MAXPATHLEN . "<br>";


echo "Versión del Sistema Operativo: " . php_uname() . "<br>";

echo "Símbolo correcto de 'Fin De Línea': " . PHP_EOL . "<br>";

echo "Include Path por defecto: " . get_include_path() . "<br>";
?>
</body>
</html>