<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<!-- 5-Ejercicio 5:
Hacer un script PHP que genere un formulario HTML que contenga los siguientes campos: nombre,
apellido, edad y el botón de submit. Debe enviar la información por método POST y debe imprimir
en pantalla el contenido de lo que introdujo el usuario.
• Se deben usar las cadenas HEREDOC. -->

     <h1>Formulario</h1>
    <form method="post" action="ej5_post.php">
        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre" required><br>

        <label for="apellido">Apellido:</label>
        <input type="text" name="apellido" required><br>

        <label for="edad">Edad:</label>
        <input type="number" name="edad" required><br>

        <input type="submit" value="Enviar">
    </form>
</body>
</html>