<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<!-- 3- Ejercicio 3:
• Generar un script PHP que cree una tabla HTML con los números pares que existen entre 1
y N.
• El número N estará definido por una constante PHP. El valor de la constante N debe ser un
número definido por el alumno.
El script PHP debe estar embebido en una página HTML. -->
<?php
    
    define('N', 20); // Cambia este valor por el que desees

    
    echo '<table border="1">';
    echo '<tr><th>Números Pares</th></tr>';

    
    for ($i = 2; $i <= N; $i += 2) {
        echo '<tr><td>' . $i . '</td></tr>';
    }

    
    echo '</table>';
    ?>
</body>
</html>