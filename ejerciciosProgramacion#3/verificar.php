<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $usuario = $_POST["usuario"];
    $contrasena = $_POST["contrasena"];
    
    // Leer el archivo de datos de usuarios
    $archivoUsuarios = "usuarios.txt";
    $usuarios = file($archivoUsuarios, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    
    // Verificar las credenciales ingresadas
    $credencialesCorrectas = false;
    foreach ($usuarios as $linea) {
        list($usuarioGuardado, $contrasenaGuardada) = explode(":", $linea);
        if ($usuario === trim($usuarioGuardado) && $contrasena === trim($contrasenaGuardada)) {
            $credencialesCorrectas = true;
            break;
        }
    }
    
    if ($credencialesCorrectas) {
        // Redireccionar a la página de bienvenida si las credenciales son correctas
        header("Location: bienvenida.php");
        exit;
    } else {
        // Mostrar un mensaje de error si las credenciales son incorrectas
        echo "Usuario o contraseña incorrectos. <a href='ej22.php'>Volver al inicio</a>";
    }
} else {
    echo "Acceso no autorizado.";
}
?>
