<!-- Representar una estructura de árbol n-ario utilizando arrays de PHP. Representar un árbol similar
al de la figura.
RSTDUVGFHJ
LQWCKAXZI
-->
<?php
class Nodo {
    public $valor;
    public $relaciones;
    public function __construct($valor) {
        $this->valor = $valor;
        $this->relaciones = [];
    }
}

class Arbol {
    public $raiz;

    public function __construct() {
        $this->raiz = null;
    }

   
}

$nodoR = new Nodo("R");
$nodoS = new Nodo("S");
$nodoT = new Nodo("T");
$nodoD = new Nodo("D");
$nodoU = new Nodo("U");
$nodoV = new Nodo("V");
$nodoG = new Nodo("G");
$nodoF = new Nodo("F");
$nodoH = new Nodo("H");
$nodoJ = new Nodo("J");
$nodoL = new Nodo("L");
$nodoQ = new Nodo("Q");
$nodoW = new Nodo("W");
$nodoC = new Nodo("C");
$nodoK = new Nodo("K");
$nodoA = new Nodo("A");
$nodoX = new Nodo("X");
$nodoZ = new Nodo("Z");
$nodoI = new Nodo("I");

// armamos el arbol
$nodoR->relaciones[] = $nodoS;
$nodoR->relaciones[] = $nodoT;
$nodoR->relaciones[] = $nodoD;

$nodoS->relaciones[] = $nodoU;
$nodoS->relaciones[] = $nodoV;

$nodoU->relaciones[] = $nodoL;
$nodoU->relaciones[] = $nodoQ;
$nodoU->relaciones[] = $nodoW;

$nodoT->relaciones[] = $nodoG;
$nodoT->relaciones[] = $nodoF;

$nodoG->relaciones[] = $nodoC;
$nodoG->relaciones[] = $nodoK;

$nodoH->relaciones[] = $nodoA;
$nodoH->relaciones[] = $nodoX;


$nodoJ->relaciones[] = $nodoZ;
$nodoJ->relaciones[] = $nodoI;

$nodoD->relaciones[] = $nodoH;
$nodoD->relaciones[] = $nodoJ;

$arbol = new Arbol();

$arbol->raiz = $nodoR;

echo "<pre>";
print_r($arbol);
echo "</pre>";

?>
