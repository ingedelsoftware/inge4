<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
    $nombre = $_POST["nombre"];
    $password = $_POST["password"];
    $pais = $_POST["pais"];
    $intereses = isset($_POST["intereses"]) ? $_POST["intereses"] : [];
    $suscripcion = isset($_POST["suscripcion"]) ? "Sí" : "No";
    $tomaTereré = isset($_POST["tomaTereré"]) ? $_POST["tomaTereré"] : "";
    $comentarios = $_POST["comentarios"];
    
    
    echo "<h1>Datos Introducidos por el Usuario</h1>";
    echo "<p><strong>Nombre:</strong> $nombre</p>";
    echo "<p><strong>Contraseña:</strong> $password</p>";
    echo "<p><strong>País:</strong> $pais</p>";
    echo "<p><strong>Intereses:</strong> " . implode(", ", $intereses) . "</p>";
    echo "<p><strong>Suscripcion:</strong> $suscripcion</p>";
    echo "<p><strong>Toma Tereré:</strong> $tomaTereré</p>";
    echo "<p><strong>Comentarios:</strong> $comentarios</p>";
} else {
    echo "Acceso no autorizado.";
}
echo "<br><br>";
echo "<a href='ej21.php'>Volver</a>"
?>
