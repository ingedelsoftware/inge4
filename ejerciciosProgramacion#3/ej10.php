<!-- 10-Ejercicio 10:
Hacer un script PHP que declare un vector de 50 elementos con valores aleatorios enteros entre 1
y 1000. El script debe determinar cuál es el elemento con mayor valor en el vector, se debe
imprimir un mensaje con el valor y el índice en donde se encuentra.
Mensaje de ejemplo: El elemento con índice 8 posee el mayor valor que 789.
Obs: El alumno deberá crear sus propias funciones para realizar este ejercicio. -->
<?php

function generarVectorAleatorio() {
    $vector = array();
    
    for ($i = 0; $i < 50; $i++) {
        $vector[] = rand(1, 1000);
    }
    
    return $vector;
}


function encontrarMayor($vector) {
    $maxValor = $vector[0];
    $maxIndice = 0;
    
    for ($i = 1; $i < count($vector); $i++) {
        if ($vector[$i] > $maxValor) {
            $maxValor = $vector[$i];
            $maxIndice = $i;
        }
    }
    
    return array('valor' => $maxValor, 'indice' => $maxIndice);
}


$vectorAleatorio = generarVectorAleatorio();


$mayorElemento = encontrarMayor($vectorAleatorio);


echo "El elemento con índice {$mayorElemento['indice']} posee el mayor valor que {$mayorElemento['valor']}.";
?>
