<!-- 21- Ejercicio 21
Hacer un formulario HTML que posee los siguientes tipos de elementos (el alumno debe crear un
formulario que tenga sentido):
• Un campo de Texto
• Un campo Password
• Un select Simple
• Un select múltiple
• Un Checkbox
• Un Radio Button
• Un Text Area
Implementar un script PHP que capture los datos del anterior formulario e imprima en pantalla lo
que introdujo el usuario de manera legible. -->
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario HTML</title>
</head>
<body>
    <h1>Formulario HTML</h1>
    <form action="procesar.php" method="POST">
        
        <label for="nombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre" required>
        <br>
        
        
        <label for="password">Contraseña:</label>
        <input type="password" id="password" name="password" required>
        <br>
        
        
        <label for="pais">País:</label>
        <select id="pais" name="pais">
            <option value="paraguay">Paraguay</option>
            <option value="usa">Estados Unidos</option>
            <option value="canada">Canadá</option>
            <option value="mexico">México</option>
            <option value="espana">España</option>
        </select>
        <br>
        
        
        <label for="intereses">Intereses:</label>
        <select id="intereses" name="intereses[]" multiple>
            <option value="anime">Anime</option>
            <option value="deporte">Deporte</option>
            <option value="musica">Música</option>
            <option value="lectura">Lectura</option>
            <option value="viajes">Viajes</option>
        </select>
        <br>
        
        
        <label for="suscripcion">Suscribirse:</label>
        <input type="checkbox" id="suscripcion" name="suscripcion" value="1">
        <br>
        
        
        <label>Toma Tereré?</label>
        <input type="radio" id="si" name="tomaTereré" value="masculino">
        <label for="masculino">Si</label>
        <input type="radio" id="no" name="tomaTereré" value="femenino">
        <label for="femenino">NO</label>
        <br>
        
        
        <label for="comentarios">Comentarios:</label>
        <textarea id="comentarios" name="comentarios" rows="4" cols="50"></textarea>
        <br>
        
        <input type="submit" value="Enviar">
    </form>
</body>
</html>
