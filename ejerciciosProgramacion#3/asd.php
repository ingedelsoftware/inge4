<?php
class Nodo {
    public $valor;
    public $izquierda;
    public $derecha;

    public function __construct($valor) {
        $this->valor = $valor;
        $this->izquierda = null;
        $this->derecha = null;
    }
}

class ArbolBinario {
    public $raiz;

    public function __construct() {
        $this->raiz = null;
    }

    
    public function inorden($nodo) {
        if ($nodo !== null) {
            $this->inorden($nodo->izquierda);
            echo $nodo->valor . " ";
            $this->inorden($nodo->derecha);
        }
    }
    public function preorden($nodo) {
        if ($nodo !== null) {
            echo $nodo->valor . " ";
            $this->preorden($nodo->izquierda);
            $this->preorden($nodo->derecha);
        }
    }
    public function postorden($nodo) {
        if ($nodo !== null) {
            $this->postorden($nodo->izquierda);
            $this->postorden($nodo->derecha);
            echo $nodo->valor . " ";
        }
    }
}


$nodo3 = new Nodo(3);
$nodo6 = new Nodo(6);
$nodo4 = new Nodo(4);
$nodo14 = new Nodo(14);
$nodo9 = new Nodo(9);
$nodo900 = new Nodo(900);
$nodo45 = new Nodo(45);
$nodo100 = new Nodo(100);
$nodo30 = new Nodo(30);
$nodo40 = new Nodo(40);
$nodo15 = new Nodo(15);


$nodo3->izquierda = $nodo6;
$nodo3->derecha = $nodo4;
$nodo6->izquierda = $nodo14;
$nodo6->derecha = $nodo9;
$nodo4->derecha = $nodo45;
$nodo4->izquierda = $nodo900;
$nodo14->izquierda = $nodo100;
$nodo14->derecha = $nodo30;
$nodo9->izquierda = $nodo40;
$nodo9->derecha = $nodo15;


$arbol = new ArbolBinario();
$arbol->raiz = $nodo3;


echo "Recorrido en orden (inorden) del árbol binario: ";
$arbol->inorden($arbol->raiz);
echo "<br>";
echo "Recorrido en preorden del árbol binario: ";
$arbol->preorden($arbol->raiz);
echo "<br>";
echo "Recorrido en postorden del árbol binario: ";
$arbol->postorden($arbol->raiz);

?>
