<!-- 9- Ejercicio 9:
Realizar un script en PHP que declare un vector de 100 elementos con valores aleatorios enteros
entre 1 y 100 y sume todos los valores. El script debe imprimir un mensaje con la sumatoria de los
elementos
Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. -->
<?php

function generarVectorAleatorio() {
    $vector = array();
    
    for ($i = 0; $i < 100; $i++) {
        $vector[] = rand(1, 100);
    }
    
    return $vector;
}


function sumarVector($vector) {
    $sumatoria = 0;
    
    foreach ($vector as $valor) {
        $sumatoria += $valor;
    }
    
    return $sumatoria;
}


$vectorAleatorio = generarVectorAleatorio();


$sumatoria = sumarVector($vectorAleatorio);


echo "La sumatoria de los elementos del vector es: $sumatoria";
?>
