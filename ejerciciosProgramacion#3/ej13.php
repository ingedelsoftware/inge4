<!-- 13-Ejercicio 13
Crear un array asociativo de 10 elementos. Los valores de los 10 elemento del array deben ser
strings.
Crear un array asociativo de un elemento. El valor del array debe ser un string.
El script PHP debe hacer lo siguiente:
• Imprimir en pantalla si existe la clave del array de un elemento en el array de 10
elementos (si no existe, se imprime un mensaje).
• Imprimir en pantalla si existe el valor del vector de un elemento en el vector de 10
elementos (si no existe, se imprime un mensaje).
• Si no existe ni la clave ni el valor (del vector de un elemento) en el vector mayor se inserta
como un elemento más del vector. -->
<?php

$array10 = array(
    'clave1' => 'valor1',
    'clave2' => 'valor2',
    'clave3' => 'valor3',
    'clave4' => 'valor4',
    'clave5' => 'valor5',
    'clave6' => 'valor6',
    'clave7' => 'valor7',
    'clave8' => 'valor8',
    'clave9' => 'valor9',
    'clave10' => 'valor10'
);


$array1 = array(
    'nuevaClave' => 'nuevoValor'
);


$clave1Existente = array_key_exists(key($array1), $array10);


$valor1Existente = in_array(current($array1), $array10);


if (!$clave1Existente && !$valor1Existente) {
    $array10[key($array1)] = current($array1);
}


echo "Verificar clave del array de un elemento en el array de 10 elementos:<br>";
if ($clave1Existente) {
    echo "La clave existe en el array de 10 elementos.<br>";
} else {
    echo "La clave no existe en el array de 10 elementos.<br>";
}

echo "Verificar valor del array de un elemento en el array de 10 elementos:<br>";
if ($valor1Existente) {
    echo "El valor existe en el array de 10 elementos.<br>";
} else {
    echo "El valor no existe en el array de 10 elementos.<br>";
}

echo "Array de 10 elementos actualizado:<br>";
print_r($array10);
?>
