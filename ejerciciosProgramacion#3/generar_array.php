<?php
function generarArrayAleatorio($numElementos) {
    $array = array();

    for ($i = 0; $i < $numElementos; $i++) {
        $indice = generarIndiceAleatorio();
        $valor = rand(1, 1000);
        $array[$indice] = $valor;
    }

    return $array;
}

function generarIndiceAleatorio() {
    $longitud = rand(5, 10);
    $caracteres = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $indice = '';

    for ($i = 0; $i < $longitud; $i++) {
        $indice .= $caracteres[rand(0, strlen($caracteres) - 1)];
    }

    return $indice;
}
?>
