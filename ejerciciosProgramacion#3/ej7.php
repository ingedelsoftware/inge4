<!-- 7- Ejercicio 7:
Hacer un script en PHP que determine si una cadena dada es un palíndromo
Un palíndromo (del griego palin dromein, volver a ir hacia atrás) es una palabra, número o frase
que se lee igual hacia adelante que hacia atrás. Si se trata de un número, se llama capicúa.
Habitualmente, las frases palindrómicas se resienten en su significado cuanto más largas son
Ejemplos: radar, Neuquén, anilina, ananá, Malayalam
Obs: El alumno deberá crear sus propias funciones para realizar este ejercicio. -->
<?php
function esPalindromo($cadena) {
    
    
    
    $acentos = [ "á", "é", "í", "ó"];
    $vocales = [ "a", "e", "i", "o"];
    

    foreach ($acentos as $index => $acento) {
        $cadena = str_replace($acento, $vocales[$index], $cadena);
    }
    
    $cadena = strtolower(preg_replace('/[^a-zA-Z0-9]/', '', $cadena));
    
    $longitud = strlen($cadena);
    
    
    $inicio = 0;
    $fin = $longitud - 1;
    
    
    
    while ($inicio < $fin) {
        
        if ($cadena[$inicio] != $cadena[$fin]) {
            
            return false;
        }
        
        
        $inicio++;
        $fin--;
    }
    
    
    return true;
}


$cadenas = ["radar","Neuquén","anilina","ananá","Malayalam"];

foreach ($cadenas as $cadena){
    if (esPalindromo($cadena)) {
        echo "$cadena es un palíndromo.";
    } else {
        echo "$cadena no es un palíndromo.";
    }
    echo "<br>";
}
?>