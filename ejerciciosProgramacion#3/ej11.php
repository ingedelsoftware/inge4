<!-- 11-Ejercicio 11:
Realizar un script PHP que haga lo siguiente:
Declare un vector indexado cuyos valores sean urls (esto simula un log de acceso de un usuario).
El script debe contar e imprimir cuantas veces accedió el usuario al mismo url (no se deben repetir
los url). -->
<?php

$logAcceso = array(
    'https://www.abc.com.py/', 'https://mail.google.com', 'https://mail.google.com', 
    'https://gitref.org', 'https://mail.google.com','https://aulavirtual.uc.edu.py/aulas',
    'https://hattrick.org','https://aulavirtual.uc.edu.py/aulas','https://aulavirtual.uc.edu.py/aulas','https://www.abc.com.py/'
);///en esta variable escribir url de la imagen


$conteoAccesos = array();


foreach ($logAcceso as $url) {
    if (isset($conteoAccesos[$url])) {
        $conteoAccesos[$url]++;
    } else {
        $conteoAccesos[$url] = 1;
    }
}


echo "Conteo de accesos a cada URL:<br>";
foreach ($conteoAccesos as $url => $conteo) {
    echo "$url: $conteo veces<br>";
}
?>
