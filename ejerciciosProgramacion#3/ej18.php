<!-- Realizar un contador de visitas en PHP utilizando un archivo para mantener la cantidad de visitas a
la página.
Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. -->
<?php

function actualizarContador() {
    $archivo = 'contador.txt';

    if (file_exists($archivo)) {
        
        $contador = intval(file_get_contents($archivo));

        
        $contador++;

        
        file_put_contents($archivo, $contador);

        return $contador;
    } else {
        
        file_put_contents($archivo, 1);
        return 1;
    }
}


$contadorActual = actualizarContador();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contador de Visitas</title>
</head>
<body>
    <h1>Bienvenido a nuestra página web</h1>
    <p>Esta página ha sido visitada <?php echo $contadorActual; ?> veces.</p>
</body>
</html>
