<?php
function imprimirClavesFiltradas($array) {
    $clavesFiltradas = array_filter(array_keys($array), function ($clave) {
        return preg_match('/^[admz]/i', $clave);
    });

    if (empty($clavesFiltradas)) {
        echo "No se encontraron claves que comiencen con las letras 'a', 'd', 'm' o 'z'.";
    } else {
        echo "Claves que comienzan con 'a', 'd', 'm' o 'z': " . implode(', ', $clavesFiltradas);
    }
}
?>
