<!-- Implementar un script PHP que implemente una funcionalidad básica de login
• El formulario debe tener dos campos: usuario, contraseña.
• El formulario deberá tener un botón de login.
• La verificación de usuario y contraseña se realizará a través de un archivo de datos (ya sea
.txt, .dat).
• Si el usuario introducido existe en el archivo, se debe re-enviar a una página de
bienvenida.
• Si el usuario introducido no existe en el archivo, se debe imprimir un mensaje de error de
acceso. -->
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iniciar Sesión</title>
</head>
<body>
    <h1>Iniciar Sesión</h1>
    <form action="verificar.php" method="POST">
        <label for="usuario">Usuario:</label>
        <input type="text" id="usuario" name="usuario" required>
        <br>
        <label for="contrasena">Contraseña:</label>
        <input type="password" id="contrasena" name="contrasena" required>
        <br>
        <input type="submit" value="Iniciar Sesión">
    </form>
</body>
</html>
