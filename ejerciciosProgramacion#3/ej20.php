<!-- 20-Ejercicio 20
Leer un archivo agenda.txt (creado por el alumno), este archivo contendrá el nombre y el apellido
de personas.
Se debe crear una función PHP que busque un nombre y un apellido en dicho archivo e imprima un
mensaje si se encontró o no se encontró el nombre y apellido en el archivo. Nombre y apellido son
variables que se debe introducir el usuario por formulario.
Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. -->



<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buscar Nombre y Apellido</title>
</head>
<body>
    <h1>Buscar Nombre y Apellido en la Agenda</h1>
    <form action="buscar.php" method="POST">
        <label for="nombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre" required>
        <br>
        <label for="apellido">Apellido:</label>
        <input type="text" id="apellido" name="apellido" required>
        <br>
        <input type="submit" value="Buscar">
    </form>
</body>
</html>
