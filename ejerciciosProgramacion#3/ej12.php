<!-- 12- Ejercicio 12
Hacer un script PHP que haga lo siguiente:
• Crear un array indexado con 10 cadenas.
• Crear una variable que tenga un string cualquiera como string.
El script debe:
• Buscar si la cadena declarada está en el vector declarado.
• Si esta, se imprime “Ya existe” la cadena.
• Si no está, se imprime “Es Nuevo” y se agrega al Final del array.
Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. -->
<?php

$cadenas = array(
    'cadena1',
    'cadena2',
    'cadena3',
    'cadena4',
    'cadena5',
    'cadena6',
    'cadena7',
    'cadena8',
    'cadena9',
    'cadena10'
);


$nuevaCadena = 'cadena11';


function buscarCadenaEnArray($cadena, $array) {
    foreach ($array as $valor) {
        if ($valor === $cadena) {
            return true;
        }
    }
    return false;
}


if (buscarCadenaEnArray($nuevaCadena, $cadenas)) {
    echo "Ya existe la cadena '$nuevaCadena' en el array.";
} else {
    echo "Es Nuevo";
    
    $cadenas[] = $nuevaCadena;
}


echo "<br>Array actualizado:";
print_r($cadenas);
?>
