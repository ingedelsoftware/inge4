<!DOCTYPE html>
<html>
<head>
    <title>Calculadora</title>
</head>
<body>
    <!-- Hacer un script en PHP que simule una calculadora con las operaciones básicas: suma, resta,
multiplicación y división.
• El usuario debe introducir el operando 1.
• El usuario debe introducir el operando 2.
• El usuario debe seleccionar la operación que quiere realizar (a través de un select).
• El usuario debe presionar el botón calcular.
• El script debe calcular el resultado de la operación e imprimirlo en pantalla. El script debe
realizar los controles respectivos (para casos donde las operaciones no son válidas). -->
    <h1>Calculadora</h1>
    <form method="post" action="">
        <label for="operando1">Operando 1:</label>
        <input type="number" name="operando1" required>
        <br><br>
        
        <label for="operando2">Operando 2:</label>
        <input type="number" name="operando2" required>
        <br><br>
        
        <label for="operacion">Operación:</label>
        <select name="operacion" required>
            <option value="suma">Suma</option>
            <option value="resta">Resta</option>
            <option value="multiplicacion">Multiplicación</option>
            <option value="division">División</option>
        </select>
        <br><br>
        
        <input type="submit" name="calcular" value="Calcular">
    </form>

    <?php
    if (isset($_POST['calcular'])) {
        $operando1 = $_POST['operando1'];
        $operando2 = $_POST['operando2'];
        $operacion = $_POST['operacion'];

        if ($operacion == "suma") {
            $resultado = $operando1 + $operando2;
        } elseif ($operacion == "resta") {
            $resultado = $operando1 - $operando2;
        } elseif ($operacion == "multiplicacion") {
            $resultado = $operando1 * $operando2;
        } elseif ($operacion == "division") {
            if ($operando2 != 0) {
                $resultado = $operando1 / $operando2;
            } else {
                echo "<p style='color: red;'>Error: No se puede dividir por cero.</p>";
            }
        }

        echo "<p>Resultado: $resultado</p>";
    }
    ?>
</body>
</html>
