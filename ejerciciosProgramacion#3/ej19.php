<!-- 19-Ejercicio 19
Dado un archivo con la siguiente información: una lista de matrículas, nombre, apellido y 3 notas
parciales para un grupo alumnos (el archivo puede tener un número variable de líneas de texto).
Procesar estos datos y construir un nuevo archivo que contenga solamente la matrícula y la
sumatoria de notas de los alumnos. -->
<?php

function calcularSumaNotas($nota1, $nota2, $nota3) {
    return $nota1 + $nota2 + $nota3;
}


$archivoEntrada = 'datos_alumnos.txt';
$archivoSalida = 'suma_notas_alumnos.txt';
if (file_exists($archivoEntrada)) {
    $handleEntrada = fopen($archivoEntrada, 'r');
}else{
    echo "Error al abrir el archivo de entrada.";
    exit;
}


if ($handleEntrada) {
    if (file_exists($archivoSalida)) {
        $handleSalida = fopen($archivoSalida, 'w');
    }
    else{
        echo "Error al abrir el archivo de salida.";
        exit;
    }

    
    if ($handleSalida) {
        
        while (($linea = fgets($handleEntrada)) !== false) {
            
            $parts = explode(' ', $linea);
            
            
            if (count($parts) === 6) {
                $matricula = $parts[0];
                $nombre = $parts[1];
                $apellido = $parts[2];
                $nota1 = floatval($parts[3]);
                $nota2 = floatval($parts[4]);
                $nota3 = floatval($parts[5]);
                
                
                $sumaNotas = calcularSumaNotas($nota1, $nota2, $nota3);
                
                
                fwrite($handleSalida, "$matricula $sumaNotas\n");
            }
        }
        
        
        fclose($handleSalida);
        
        echo "Proceso completado. Se ha creado el archivo '$archivoSalida' con las sumas de notas de los alumnos.";
    } else {
        echo "Error al abrir el archivo de salida.";
    }

    
    fclose($handleEntrada);
} else {
    echo "Error al abrir el archivo de entrada.";
}
?>
