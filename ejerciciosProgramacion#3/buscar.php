<?php

function buscarNombreApellido($nombre, $apellido) {
    $archivo = 'agenda.txt';
    
    if (!file_exists($archivo)) {
        echo "El archivo '$archivo' no existe.";
        exit;
    }
    $lineas = file($archivo, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    
    
    foreach ($lineas as $linea) {
        
        list($nombreArchivo, $apellidoArchivo) = explode(' ', $linea);
        
        
        if (trim($nombreArchivo) === $nombre && trim($apellidoArchivo) === $apellido) {
            return true; 
        }
    }
    
    return false; 
}


$nombreBuscado = isset($_POST['nombre']) ? $_POST['nombre'] : '';
$apellidoBuscado = isset($_POST['apellido']) ? $_POST['apellido'] : '';


$encontrado = buscarNombreApellido($nombreBuscado, $apellidoBuscado);


if ($encontrado) {
    echo "Se encontró el nombre '$nombreBuscado' y el apellido '$apellidoBuscado' en la agenda.";
} else {
    echo "No se encontró el nombre '$nombreBuscado' y el apellido '$apellidoBuscado' en la agenda.";
}
echo "<br><br>";
echo "<a href='ej20.php'>Volver</a>"
?>
