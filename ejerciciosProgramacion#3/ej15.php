<!-- 15- Ejercicio 15
Declarar un vector de 20 elementos con valores aleatorios de 1 a 10.
Crear una función recursiva en PHP que recorra el array de atrás para adelante. Se deberá
imprimir desde el último elemento del array hasta el primero
Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. -->

<?php

function generarVectorAleatorio() {
    $vector = array();
    
    for ($i = 0; $i < 20; $i++) {
        $vector[] = rand(1, 10);
    }
    
    return $vector;
}


function imprimirVectorReversa($vector, $indice) {
    if ($indice >= 0) {
        echo $vector[$indice] . " ";
        imprimirVectorReversa($vector, $indice - 1);
    }
}


$vectorAleatorio = generarVectorAleatorio();


echo "Vector en reversa: ";
imprimirVectorReversa($vectorAleatorio, count($vectorAleatorio) - 1);
?>
