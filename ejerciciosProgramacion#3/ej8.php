<!-- 8- Ejercicio 8:


Hacer un script en PHP que genere una matriz de dimensión n*m con números aleatorios. Las
dimensiones n (filas) y m (columnas) son variables que debe definir el alumno. La matriz generada
se debe imprimir en pantalla de manera tabular.
Obsevación: El alumno deberá crear sus propias funciones para realizar este ejercicio. -->
<?php


function generarMatrizAleatoria($n, $m) {
    $matriz = array();

    for ($i = 0; $i < $n; $i++) {
        for ($j = 0; $j < $m; $j++) {
            $matriz[$i][$j] = rand(1, 100); 
        }
    }

    return $matriz;
}


function imprimirMatriz($matriz) {
    echo "<table border='1'>";
    foreach ($matriz as $fila) {
        echo "<tr>";
        foreach ($fila as $valor) {
            echo "<td>$valor</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}


$n = 4; 
$m = 3; 


$matrizAleatoria = generarMatrizAleatoria($n, $m);


echo "<h2>Matriz Aleatoria $n x $m</h2>";
imprimirMatriz($matrizAleatoria);
?>
