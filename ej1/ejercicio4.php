<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabla de Gaseosas</title>
    <style>
        table {
            border-collapse: collapse;
            margin: 20px auto;
        }
        th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: center;
        }
        th {
            background-color: gray;
        }
        .header-row th {
            background-color: yellow;
        }
        tr:nth-child(even) {
            background-color:#90EE90; 
        }
    </style>
</head>
<body>

    <?php

    // Genera la tabla usando los datos
    echo "<table>";
    echo "<tr class='header-row'>";
    echo "<th colspan='3'>Productos</th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th>Nombre</th>";
    echo "<th>Cantidad</th>";
    echo "<th>Precio</th>";
    echo "</tr>";

    // Datos de las gaseosas
    $gaseosas = array(
        array("Coca Cola", 100, "4.500"),
        array("Pepsi", 30, "4.800"),
        array("Sprite", 20, "4.500"),
        array("Guaraná", 200, "4.500"),
        array("SevenUp", 24, "4.800"),
        array("Mirinda Naranja", 56, "4.800"),
        array("Mirinda Guaraná", 89, "4.800"),
        array("Fanta Naranja", 10, "4.500"),
        array("Fanta Piña", 2, "4.500")
    );

    // Recorremos el array e imprimimos los datos
    foreach ($gaseosas as $gaseosa) {
        echo "<tr>";
        echo "<td>{$gaseosa[0]}</td>";
        echo "<td>{$gaseosa[1]}</td>";
        echo "<td>{$gaseosa[2]}</td>";
        echo "</tr>";
    }
    ?>

</body>
</html>
