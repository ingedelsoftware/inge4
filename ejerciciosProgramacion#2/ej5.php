<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<!-- 5- Ejercicio 5:
Hacer un script PHP el cual utilice el operador ternario de PHP para realizar lo siguiente:
• Se deben declarar tres variables y le asignan valores enteros aleatorios (los valores deben
estar entre 99 y 999). Las variables serán $a, $b y $c
• Si la expresión $a*3 > $b+$c se debe imprimir que la expresión $a*3 es mayor que la
expresión $b+$c
• Si la expresión $a*3 <= $b+$c se debe imprimir que la expresión $b+$c es mayor o igual
que la expresión $a*3 -->
    <?php
        $a = rand(99,999);
        $b = rand(99,999);
        $c = rand(99,999);
        echo "a = $a <br>";
        echo "b = $b <br>";
        echo "c = $c <br>";
        echo "<br>";
        echo $a*3 > $b+$c ? "La expresión $a*3 es mayor que la expresión $b+$c" : "La expresión $b+$c es mayor o igual que la expresión $a*3";
    ?>
</body>
</html>