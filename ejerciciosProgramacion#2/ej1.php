<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <!--Observación: Todos los ejercicios deben ser subidos al gitlab.-->
<!--1- Ejercicio 1:-->
<!--Hacer un script PHP que haga lo siguiente:-->
<!--• El script PHP debe estar embebido en una página HTML-->
<!--• Crear una variable que almacene su nombre y apellido.-->
<!--• Crear una variable que guarde su nacionalidad.-->
<!--• Usar la función echo para imprimir en pantalla el contenido de la variable que almacena su-->
<!--nombre y apellido. El contenido se debe desplegar en negrita y en color rojo.-->
<!--• Usar la función print para imprimir en pantalla el contenido de la variable que almacena su-->
<!--nacionalidad. El contenido se debe desplegar subrayado.-->


<?php 
    $alumnos = [["Mateo Denis Martinez","Paraguayo"],["Stefano Modica","Paraguayo"],["Juan Jose Yegros","Argentino"]];
?>
<u><?php 
    foreach($alumnos as $alumno){
        echo "<b style='color:red;'>".$alumno[0]."</b><br>";
        echo "<u>".$alumno[1]."</u>";
    }
?></u>
</body>
</html>