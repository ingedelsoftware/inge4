<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<!-- Hacer un script en PHP que haga lo siguiente:
• El script PHP debe estar embebido en una página HTML
• Crear una variable con el siguiente contenido 24.5
• Determinar cuál es el tipo de datos que posee esta variable e imprimirlo en pantalla
(poner un carácter de nueva línea)
• En la siguiente línea del script, modificar el contenido de la variable al valor “HOLA”
• Determinar cuál es el tipo de datos que posee esta variable e imprimirlo en pantalla
(poner un carácter de nueva línea)
• Setear el tipo de la variable que contiene “HOLA” a un tipo entero
• Determinar con var_dump el contenido y tipo de esa variable -->
<?php 
    $var = 24.5;
    echo gettype($var) . "<br>";
    $var = "HOLA";
    echo gettype($var) . "<br>";
    settype($var, "integer");
    echo gettype($var) . "<br>";
?>

</body>
</html>