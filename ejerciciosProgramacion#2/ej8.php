<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<!-- 8- Ejercicio 8:
Hacer un script en PHP que genere números aleatorios hasta que el número generado sea divisible
por 983. Cuando ocurra esta condición imprimir un mensaje.
Se debe usar un ciclo infinito -->
    <?php
        $num = rand(1, 1000000);
        while($num % 983 != 0){
            $num = rand(1, 1000000);
        }
        echo "El numero $num es divisible por 983";
    ?>
</body>
</html>