<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<!-- 4-Ejercicio 4:
• El script PHP debe estar embebido en una página HTML
• Hacer un script en PHP que muestre en pantalla la tabla de multiplicar el 9, coloreando las
filas alternando gris y blanco -->
<font color='black'>
<?php 
    $color = "white";
    echo "<table border=1>";
    for ($i=0; $i <= 10; $i++) { 
        if ($i % 2 == 0) {
            $color = "white";
        }else{
            $color = "grey";
        }
        echo "<tr style='background-color:$color'>";
        echo "<td>9</td>";
        echo "<td>x</td>";
        echo "<td>$i</td>";
        echo "<td>=</td>";
        echo "<td>" . ($i * 9) . "</td>";

        echo "</tr>";
    }
    echo "</table>";
?>
</font>
</body>
</html>