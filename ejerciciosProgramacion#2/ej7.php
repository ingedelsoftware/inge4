<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<!-- 7- Ejercicio 7:
Hacer un script en PHP que imprima 900 números aleatorios pares
Se deben generar números aleatorios entre 1 y 10.000 -->

    <?php
        $numeros = array();
        $contador = 0;
        while($contador < 900){
            $numero = rand(1,10000);
            if($numero % 2 == 0){
                array_push($numeros,$numero);
                $contador++;
            }
        }
        foreach($numeros as $numero){
            echo $numero."<br>";
        }
    ?>
</body>
</html>