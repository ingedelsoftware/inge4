
@foreach ($inputs as $input)
    @php
        $type = $input['type'];
    @endphp
    
    @if($type =='text')
        <div class="form-group">
            <label for="{{$input['name']}}">{{$input['label']}}</label>
            <input type="{{$input['type']}}" name="{{$input['name']}}" id="{{$input['id']}}"
            class="{{$input['class']}}" placeholder="{{$input['placeholder']}}" value="{{old($input['name'],$input['value'])}}"
            @if($input['required']) required @endif
            @if($input['disabled']) disabled @endif
            @if($input['readonly']) readonly @endif
            >
        </div>
    @elseif($type == 'password')
        <div class="form-group">
            <label for="{{$input['name']}}">{{$input['label']}}</label>
            <input type="{{$input['type']}}" name="{{$input['name']}}" id="{{$input['id']}}"
            class="{{$input['class']}}" placeholder="{{$input['placeholder']}}" value="{{$input['value']}}"
            @if($input['required']) required @endif
            @if($input['disabled']) disabled @endif
            @if($input['readonly']) readonly @endif
            >
            @elseif($type == "datetime")
        </div>
        @elseif($type == "date")
        @elseif($type == "time")
        @elseif($type == "email")
        @elseif($type == "number")
        @elseif($type == "range")
        @elseif($type == "select")
        

        <div class="form-group">
            <label for="{{$input['name']}}">{{$input['label']}}</label>
            <select name="{{$input['name']}}" id="{{$input['id']}}" class="{{$input['class']}}"
            @if($input['required']) required @endif
            @if($input['disabled']) disabled @endif
            @if($input['readonly']) readonly @endif
            >
                <option value="" disabled @if(!isset($input['default'])) selected @endif>Seleccione</option>
                @if(isset($input['options_contains_array']))
                    
                    @foreach ($input['options'] as $option)
                        
                        <option value="{{$option->id}}" {{$input['value'] == $option->id ? 'selected' : ''}}>{{$option->name}}</option>
                    @endforeach
                @else
                @foreach ($input['options'] as $key => $value)
                    <option value="{{$key}}" {{$input['value'] == $key ? 'selected' : ''}}>{{$value}}</option>
                @endforeach
                @endif
            </select>
        </div>
        @endif
        @error($input['name'])
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
@endforeach