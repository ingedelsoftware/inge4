<!DOCTYPE html>

<html>
    <head>
        <title>@yield('title')</title>
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
        integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
        integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="https://cdn.datatables.net/v/bs4/jq-3.7.0/dt-1.13.8/r-2.5.0/datatables.min.css" rel="stylesheet">
 
        <script src="https://cdn.datatables.net/v/bs4/jq-3.7.0/dt-1.13.8/r-2.5.0/datatables.min.js"></script>
        <style>
            body {
                background-image: url({{ asset('storage/images/bg.jpg') }});
            }
        </style>
        @yield('style')
</head>
<body>

    <div class="row">
        @if(auth()->guard('proffessor')->check())

        @include('config.layout-proffessor')
        @elseif(auth()->guard('admin')->check())

        @include('config.layout-admin')
        @endif
        
        @if(auth()->guard('admin')->check() || auth()->guard('proffessor')->check() )
        <div class="col-10">
        @endif  
        <div class="content m-2 p-3">
            @yield('content') <!-- This will be replaced by view-specific content -->
        </div>
        @if(auth()->guard('admin')->check() || auth()->guard('proffessor')->check() )
        </div>
        @endif
    </div>
    <!-- Include your default scripts, etc. -->

</body>

@yield('scripts')
</html>