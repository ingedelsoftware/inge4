
<div class="flex-column flex-shrink-0 p-3 text-white bg-info" style="width: 280px;height: 100vh; ">
    <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
        <svg class="bi me-2" width="40" height="32">
            <use xlink:href="#bootstrap"></use>
        </svg>
        <span class="fs-4">ReportIT</span>
    </a>
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">

        <li class="nav-item">
            <button  class="btn btn-toggle " type="button" data-toggle="collapse" data-target="#referenceCollapse" aria-expanded="true" aria-controls="referenceCollapse">
                Referenciales
            </button>
            <div id="referenceCollapse" class="collapse show m-2 text-white">
                <ul class="btn-toggle-nav list-unstyled fw-normal p-1 small m-2">
                    <li class="nav-item"><a href="{{route('proffessor.ticket.index')}}" class="text-white">Ticket</a></li>
                </ul>
            </div>
        </li>        
</div>

