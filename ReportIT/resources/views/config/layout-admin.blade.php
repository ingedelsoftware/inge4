
<div class="flex-column flex-shrink-0 p-3 text-white bg-info" style="width: 280px;height: 100vh; ">
    <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
        <svg class="bi me-2" width="40" height="32">
            <use xlink:href="#bootstrap"></use>
        </svg>
        <span class="fs-4">ReportIT</span>
    </a>
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
        
        <li class="nav-item">
            <button  class="btn btn-toggle " type="button" data-toggle="collapse" data-target="#referenceCollapse" aria-expanded="true" aria-controls="referenceCollapse">
                Referenciales
            </button>
            <div id="referenceCollapse" class="collapse show m-2 text-white">
                <ul class="btn-toggle-nav list-unstyled fw-normal p-1 small m-2">
                    <li class="nav-item"><a href="{{route('admin.staff.index')}}" class="text-white">Staff</a></li>
                    <li class="nav-item"><a href="{{route('admin.classroom.index')}}" class="text-white">Clases</a></li>

                    {{-- <li class="nav-item"><a href="{{route('admin.proffessors.index')}}" class="text-white">Proffessors(En proceso)</a></li> --}}
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <button  class="btn btn-toggle" type="button" data-toggle="collapse" data-target="#dependenceCollapse" aria-expanded="true" aria-controls="dependenceCollapse">
                Dependencias
            </button>
            <div id="dependenceCollapse" class="collapse show m-2 text-white">
                <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small m-2">
                    <li><a href="{{route('admin.technology.index')}}" class="text-white">Tecnologia</a></li>
                    <li><a href="{{route('admin.ticket.index')}}" class="text-white">Tickets</a></li>
                </ul>
            </div>
        </li>
        
        
</div>

