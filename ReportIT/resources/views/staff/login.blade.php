@extends('config.default-style-doc')
@section('title', 'Staff Administrador')

@section('style')
    <style>
        /* Custom styles can be added here */
        body {
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .custom-card{
            width: 400px;
            height: 400px;
            background-color: #fff 
        }
    </style>
@endsection

@section('content')
<div class="row justify-content-center p-3">
    <div class="col-md-6">
        <div class="card custom-card">
            <div class="card-header">
                <h1>Login Staff</h1>
            </div>
            @if(session()->has('msg'))
                <div class="alert alert-danger">{{session('msg')}}</div>
            @endif
            <div class="card-body">
                <form action="{{route('admin.login')}}" method="POST">
                    @csrf
                    @include('config.create-inputs',[
                        'inputs'=>[
                            [
                                'type'=>'text',
                                'name'=>'username',
                                'label'=>'Usuario',
                                'placeholder'=>'Usuario',
                                'value'=>'',
                                'required'=>true,
                                'disabled'=>false,
                                'readonly'=>false,
                                'class'=>'form-control',
                                'id'=>'username',
                            ],
                            [
                                'type'=>'password',
                                'name'=>'password',
                                'label'=>'Password',
                                'placeholder'=>'Password',
                                'value'=>'',
                                'required'=>true,
                                'disabled'=>false,
                                'readonly'=>false,
                                'class'=>'form-control',
                                'id'=>'password',
                            ],
                        ]
                    ])
                    <div class="btn-group m-2">
                        <button type="submit" class="btn btn-primary">Login</button>
                        <!-- <button type="button" class="btn btn-secondary" onclick="open('forgot-pasword.php')">ForgotPassword</button> -->
                    </div>
                        
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <script>
        
    </script>
@endsection