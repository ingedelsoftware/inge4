<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
        integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
        
    <style>
        body {
            background-image: url({{ asset('storage/images/bg.jpg') }});
        }
    </style>
</head>

<body>
        <div class="flex-column flex-shrink-0 p-3 text-white bg-info" style="width: 280px;height: 100vh; ">
            <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
                <svg class="bi me-2" width="40" height="32">
                    <use xlink:href="#bootstrap"></use>
                </svg>
                <span class="fs-4">ReportIT</span>
            </a>
            <hr>
            <ul class="nav nav-pills flex-column mb-auto">
                
                <li class="nav-item">
                    <button  class="btn btn-toggle " type="button" data-toggle="collapse" data-target="#referenceCollapse" aria-expanded="true" aria-controls="referenceCollapse">
                        Referenciales
                    </button>
                    <div id="referenceCollapse" class="collapse show m-2 text-white">
                        <ul class="btn-toggle-nav list-unstyled fw-normal p-1 small m-2">
                            <li class="nav-item"><a href="#" class="text-white">Staff</a></li>
                            <li class="nav-item"><a href="#" class="text-white">Proffessors</a></li>
                            <li class="nav-item"><a href="#" class="text-white">Classrooms</a></li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <button  class="btn btn-toggle" type="button" data-toggle="collapse" data-target="#dependenceCollapse" aria-expanded="true" aria-controls="dependenceCollapse">
                        Dependencias
                    </button>
                    <div id="dependenceCollapse" class="collapse show m-2 text-white">
                        <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small m-2">
                            <li><a href="#" class="text-white">Technology</a></li>
                            <li><a href="#" class="text-white">Tickets</a></li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <button  class="btn btn-toggle" type="button" data-toggle="collapse" data-target="#informCollapse" aria-expanded="true" aria-controls="informCollapse">
                        Informes
                    </button>
                    <div id="informCollapse" class="collapse show m-2 text-white">
                        <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small m-2">
                            <li><a href="#" class="text-white">Tickets</a></li>
                            <li><a href="#" class="text-white">Proffessors</a></li>
                            <li><a href="#" class="text-white">Classrooms</a></li>
                        </ul>
                    </div>
                </li>
                
        </div>
        <div class="col-10">
        </div>
   
    

</body>

</html>
