@extends('config.default-style-doc')
@section('title','Listado de Clases')
@section('content')
@if(session()->has('msg'))
<div class="alert alert-success">{{session('msg')}}</div>
@endif
<div class="card">
    <div class="card-header">
        <h1>Listado de Clases</h1>
    </div>
    <div class="card-body">
        <a href="{{route('admin.classroom.create')}}" class="btn btn-primary mb-2">Crear Clase</a>

        <table id="classroomTable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
        
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#classroomTable').DataTable({
                "processing": true,
                "serverSide": true,
                ajax:{
                    url:'{{ route('admin.classroom.datatable') }}',
                    type:'post',
                    data:function(d){
                        //Parametros adicionales a enviar
                        // d.only_active = $('#check_solo_activados')[0].checked;
                        d._token = '{{ csrf_token() }}';
                    },
                },
                "columns": [
                    
                    {data: 'id'},
                    {data: 'name'},
                    {data: null, render:function(data,type,row){
                        var btns = '<button data-action="edit" data-id="'+ row.id +'" class="btn btn-primary btn-sm">Editar</button>'
                        btns +='<button data-action="delete" data-id="'+ row.id +'" class="btn btn-danger btn-sm">Eliminar</button>'
                        return btns;
                       
                    }
                    }
                ],
                dom:
                `<'clearfix'
                >`
                +
                `<'row'
                    <'col-sm-12'tr>
                >`
                +
                `<'row'
                    <'col-sm-12 col-md-5'i>
                    <'col-sm-12 col-md-7'p>
                    
                >`
            });
            
        });
        $(document).on("click", "#classroomTable tr td button", function(event) {
            var action = $(this).data("action");
            var id = $(this).data("id");
            var method;
            var route;
            switch (action) {
                case "edit":
                    method = "get";
                    route = "{{route('admin.classroom.edit','xx')}}"
                    break;
                case "delete":
                    method = "delete";
                    route = "{{route('admin.classroom.destroy','xx')}}"
                    break;
            }
            
            route = route.replace('xx',id);
            if(
                action == "delete"
            ){
                if(!confirm("¿Estas seguro de eliminar esta tecnologia?")){
                    return false;
                }else{
                    $.ajax({
                        url:route,
                        type:method,
                        data:{
                            _token:'{{csrf_token()}}'
                        },
                        success:function(data){
                            var msg;
                            if(data.code){
                                $('#ticketTable').DataTable().ajax.reload();
                                msg = "El tecnologia se ha eliminado correctamente"
                                $('#ticketTable').DataTable().ajax.reload();
                            }else
                            {
                                msg = "El tecnologia no se ha podido eliminar"
                            }
                            alert(msg);
                        }
                    });

                }      
            }else{
                
                window.location.href = route;
            }
        });
    </script>
@endsection