@extends('config.default-style-doc')
@section('title','Listado de Staff')

@section('content')
<div class="row card">
    <div class="col-12">
        <h1>Listado de Staff</h1>
        <a href="{{route('admin.staff.create')}}" class="btn btn-primary mb-2">Crear Staff</a>
        <table id="staffTable" class="table table-bordered">
            <thead>
                <tr>
                    <th>Id</th> 
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Usuario</th>
                    <th>Direccion</th>
                    <th>Telefono</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#staffTable').DataTable({
                "processing": true,
                "serverSide": true,
                ajax:{
                    url:'{{ route('admin.staff.datatable') }}',
                    type:'post',
                    data:function(d){
                        //Parametros adicionales a enviar
                        // d.only_active = $('#check_solo_activados')[0].checked;
                        d._token = '{{ csrf_token() }}';
                    },
                },
                "columns": [
                    {data:'id'},
                    {data: 'name'},
                    {data: 'lastname'},
                    {data: 'username'},
                    {data:'address'},
                    {data: 'phone'},
                    {data: null, render:function(data,type,row){
                        var btns = '<button data-action="edit" data-id="'+ row.id +'" class="btn btn-primary btn-sm">Editar</button>'
                        btns +='<button data-action="delete" data-id="'+ row.id +'" class="btn btn-danger btn-sm">Eliminar</button>'
                        return btns;
                    }
                    }
                ],
                dom:
                `<'clearfix'
                >`
                +
                `<'row'
                    <'col-sm-12'tr>
                >`
                +
                `<'row'
                    <'col-sm-12 col-md-5'i>
                    <'col-sm-12 col-md-7'p>
                    
                >`
            });
        });
    </script>
@endsection
