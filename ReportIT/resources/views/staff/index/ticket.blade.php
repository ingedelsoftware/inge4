@extends('config.default-style-doc')
@section('title', 'Listado de ticket')
@section('content')
@if(session()->has('msg'))
<div class="alert alert-success">{{session('msg')}}</div>
@endif
<div class="card">
    <div class="card-header">
        <h1>Listado de ticket</h1>
    </div>
    <div class="card-body">
        
        <table id="ticketTable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Titulo</th>
                    <th>Descripcion</th>
                    <th>Estado</th>
                    <th>Profesor</th>
                    <th>Fecha de creacion</th>
                    <th>Fecha de actualizacion</th>
                    <th>Prioridad</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
        
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#ticketTable').DataTable({
                "processing": true,
                "serverSide": true,
                ajax:{
                    url:'{{ route('admin.ticket.datatable') }}',
                    type:'post',
                    data:function(d){
                        //Parametros adicionales a enviar
                        // d.only_active = $('#check_solo_activados')[0].checked;
                        d._token = '{{ csrf_token() }}';
                    },
                },
                "columns": [
                    
                    {data: 'id'},
                    {data: 'title'},
                    {data: 'description'},
                    {data: 'status'},
                    {data: null, render:function(data,type,row,meta){
                        return row.name + ' ' + row.lastname;
                    }},
                    {data: 'created_at',render:function(data,type,row,meta){
                        return new Date(data).toLocaleString('es-PY', { day: '2-digit', month: '2-digit', year: 'numeric' });                    }},
                    {data: 'updated_at',render:function(data,type,row,meta){
                        return new Date(data).toLocaleString('es-PY', { day: '2-digit', month: '2-digit', year: 'numeric' });
                    }},
                    {data: 'priority'},
                    {data:'status'},
                    {data: null, render:function(data,type,row){
                        var btns = '<button data-action="show" data-id="'+ row.id +'" class="btn btn-primary btn-sm">Ver</button>'
                        return btns;
                    }}
                ],
                dom:
                `<'clearfix'
                >`
                +
                `<'row'
                    <'col-sm-12'tr>
                >`
                +
                `<'row'
                    <'col-sm-12 col-md-5'i>
                    <'col-sm-12 col-md-7'p>
                    
                >`
            });
        });
        $(document).on("click", "#ticketTable tr td button", function(event) {
            var action = $(this).data("action");
            var id = $(this).data("id");
            var method;
            var route;
            switch (action) {
                case "show":
                    method = "GET";
                    route = "{{route('admin.ticket.show','xx')}}";
                    break;
            }
            route = route.replace('xx',id);
            
                //redirect to edit
                window.location.href = route;
            
        });
    </script>
@endsection