@extends('config.default-style-doc')

@section('title', 'Creacion de tecnologias')



@section('title', 'Creacion de Tecnologias')
@section('content')
    <form class="card" id="the-form" action="{{route('admin.technology.store')}}" method="POST">
        @csrf
        @if(session()->has('msg'))
        <div class="alert alert-success">{{session('msg')}}</div>
        @endif
        <div class="card-header">
            <h1>Creacion de Tecnologias</h1>
        </div>
        <div class="card-body">
            <select name="type" id="type" class="form-control">
                <option value="" {{!isset($record)? 'selected' : ''}} disabled>Seleccione un tipo de tecnologia</option>
                <option  {{isset($record) ? ($record->type == 'cpu'?'selected': 'as' ) : 'asd'}} value="cpu">Computadora</option>
                <option  {{isset($record) ? ($record->type == 'infocus'?'selected': 'as' ) : 'asd'}} value="infocus">Proyector</option>
            </select> 
            <div class="d-none" id="pcspecs">
                <button type="button" class="btn btn-primary" id="addCpu"> Agregar cpu</button>
                {{-- insertar input para cpu que consuma api --}}
                <button type="button" class="btn btn-primary" id="addMb"> Agregar MB</button>
                {{-- insertar input para mb que consuma api --}}
                <button type="button" class="btn btn-primary" id="addOther">Agregar otros </button>
                {{-- insertar input para otros teclado mouse etc --}}
                <div id="cpuinfo"></div>
                <div id="mbinfo"></div>
                <div id="others"></div>

            </div>
            <div class="d-none" id="infocusinfo">
                <label for="brand">Marca</label>
                <input type="text" id="brand" name="brand" required>
                <label for="model">Modelo</label>
                <input type="text" id="model" name="model" required>
            </div>

            @include('config.create-inputs',[
                'inputs'=>[
                    [
                        'type'=>'select',
                        'name'=>'classroom_id',
                        'label'=>'Aula',
                        'placeholder'=>'Aula',
                        'value'=> isset($record) ? $record->classroom_id : '',
                        'required'=>true,
                        'disabled'=>false,
                        'readonly'=>false,
                        'class'=>'form-control',
                        'options'=>$classrooms,
                        'options_contains_array'=>true,
                        'default'=>'',
                        'id'=>'classroom_id',
                    
                    ]
                ],
                'method'=>'POST',
                'action'=>route('admin.technology.store'),
                'button'=>'Guardar'
            ])

            <button type="submit" class="btn btn-primary mt-2">Crear</button>

        </div>
    </form>
@endsection

@section('scripts')
    <script>
        $('#type').on('change',function(){
            if($(this).val() == 'cpu'){
                $('#pcspecs').removeClass('d-none');
                $('#infocusinfo').addClass('d-none');
                $('#brand').attr('required', false); 
                $('#model').attr('required', false); 
            }else if($(this).val() == 'infocus'){
                $('#infocusinfo').removeClass('d-none');
                $('#pcspecs').addClass('d-none');
                $('#brand').attr('required', true); 
                $('#model').attr('required', true); 
            }
        })
        $('#addCpu').on('click',function(e){
             e.stopImmediatePropagation();
            addCpu();
        })
        $('#addMb').on('click',function(e){
             e.stopImmediatePropagation();
            addMb();
        })
        $('#addOther').on('click',function(e){
             e.stopImmediatePropagation();
            addOther();
        })

        function addCpu(){
            
            //consumir api para obtener cpus
            cpuApi();
            $('#addCpu').prop('disabled',true);
        }
        function addMb(){
            //consumir api para obtener motherboards
            mbApi();
            $('#addMb').prop('disabled',true);

        }
        function addOther(){
            
            $('#others').append('<div class="row"><label class="col-6" type="text">Periferico</label><input class="col-6 form-control" type="text" name="other[]" id="other[]"></div>');
        }
        function edit(data){
            var specs = JSON.parse(data );
            if( $('#type').val() == 'cpu'){
                addCpu();
                addMb();
                var cpu = specs.cpu;
                var mb = specs.mb;
                var other = specs.other;
                $('#pcspecs').removeClass('d-none');
                $('#infocusinfo').addClass('d-none');
                $('#brand').prop('required',false);
                $('#model').prop('required',false);
                $.each(other, function (key, value) {
                    $('#others').append('<div class="row"><label class="col-6" type="text">Periferico</label><input class="col-6 form-control" type="text" name="other[]" id="other[]" value="'+value+'"></div>');
                });
            }else{
                var brand = specs.brand;
                var model = specs.model;
                $('#infocusinfo').removeClass('d-none');
                $('#pcspecs').addClass('d-none');
                $('#brand').val(brand);
                $('#model').val(model);
                $('#brand').prop('required',true);
                $('#model').prop('required',true);

            }
        }
        function cpuApi(){
            
            $('#cpuinfo').html('');
            $('#cpuinfo').append('<div class="row"><label type="text" class="col-6" for="cpu">Procesador </label> <select name="cpu" id="cpu" class="form-control col-6"></select></div>');
            //aca listado de cpu
            
            $.ajax({
                type: "get",
                url: "{{route('api.cpu.index')}}",
                success: function (response,responseText,xhr) {
                    if(xhr.status == 200){
                        $.each(response.data, function (key, value) {
                            @if(isset($record) && $record->type == 'cpu')
                                var specs = @json($record->specs);
                                $('#cpu').append('<option '+ ( JSON.parse(specs).cpu == value ?'selected':'') +' value="'+value+'">'+value+'</option>');
                            @else
                                $('#cpu').append('<option value="'+value+'">'+value+'</option>');

                            @endif
                        });
                        alert('api consumida')
                    }else{
                        alert('api no pudo ser consumida')
                    }
                    
                }
            });

        }
        function mbApi(){
           
            $('#mbinfo').html('');
            @if(isset($record) && $record->type == 'cpu')
            var specs = @json($record->specs);
            $('#mbinfo').append('<div class="row"><label class="col-6" type="text">Placa madre</label><input class="col-6 form-control" value="'+(JSON.parse(specs).mb)+'" name="mb" id="mb"></input></div>');
            @else
            $('#mbinfo').append('<div class="row"><label class="col-6" type="text">Placa madre</label><input class="col-6 form-control" name="mb" id="mb"></input></div>');
            @endif
           
            //aca consumir api
           

        }
        $('#the-form').on('submit',function(e){
            e.preventDefault();
            e.stopImmediatePropagation();

            var data;
            if($('#type').val() == 'cpu'){
               
                var cpu = $('#cpu').val();
                var mb = $('#mb').val();
                var other = $('input[name="other[]"]');
                var arr_other = [];
                $.each(other, function (key, value) {
                    arr_other.push(value.value);
                });
                data = {
                    type:'cpu',
                    cpu:cpu,
                    mb:mb,
                    other:arr_other
                }
            }else{
                var brand = $('#brand').val();
                var model = $('#model').val();
                data = {
                    type:'infocus',
                    brand:brand,
                    model:model
                }
            }
            
            var route = '{{!isset($record) ? 'store' :'edit'}}'
            var method = '{{!isset($record) ? 'post' :'patch'}}'
            if (route == 'edit'){
                route = "{{route('admin.technology.update',isset($record)?$record->id:'')}}"
            }else{
                route = "{{route('admin.technology.store')}}"
            }
            $.ajax({
                url:route,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                type:method,
                data:data,
                success:function(data){
                    
                    window.location.href = data.url;
                },
                error:function(data){
                    console.log(data);
                }
            })
        })
        @if(isset($record))
        edit(@json($record->specs));
        @endif
    </script>
@endsection