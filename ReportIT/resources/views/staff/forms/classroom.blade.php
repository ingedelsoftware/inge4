@extends('config.default-style-doc')

@section('title', 'Creacion de Clases')

@section('content')
<form class="card" action="{{route('admin.classroom.store')}}" method="POST">
    @csrf
    @if(session()->has('msg'))
    <div class="alert alert-success">{{session('msg')}}</div>
    @endif
    <div class="card-header">
        <h1>Creacion de Clases</h1>
    </div>
    <div class="card-body">
        @include('config.create-inputs',[
            "inputs"=>[
                [
                    'type'=>'text',
                    'name'=>'name',
                    'label'=>'Nombre',
                    'placeholder'=>'Nombre',
                    'value'=>'',
                    'required'=>true,
                    'disabled'=>false,
                    'readonly'=>false,
                    'class'=>'form-control',
                    'id'=>'name',
                ]
            ]
        ])
        <button type="submit" class="btn btn-primary">Crear</button>

    </div>
</form>

@endsection