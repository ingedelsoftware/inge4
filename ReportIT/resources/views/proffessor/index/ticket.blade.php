@extends('config.default-style-doc')
@section('title', 'Listado de ticket')
@section('content')
@if(session()->has('msg'))
<div class="alert alert-success">{{session('msg')}}</div>
@endif
<div class="card">
    <div class="card-header">
        <h1>Listado de ticket</h1>
    </div>
    <div class="card-body">
        <a href="{{route('proffessor.ticket.create')}}" class="btn btn-primary mb-2">Crear Ticket</a>

        <table id="ticketTable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Titulo</th>
                    <th>Descripcion</th>
                    <th>Aula</th>
                    <th>Estado</th>
                    <th>Fecha de creacion</th>
                    <th>Fecha de actualizacion</th>
                    <th>Prioridad</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
        
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#ticketTable').DataTable({
                "processing": true,
                "serverSide": true,
                ajax:{
                    url:'{{ route('proffessor.ticket.datatable') }}',
                    type:'post',
                    data:function(d){
                        //Parametros adicionales a enviar
                        // d.only_active = $('#check_solo_activados')[0].checked;
                        d._token = '{{ csrf_token() }}';
                    },
                },
                "columns": [
                    
                    {data: 'id'},
                    {data: 'title'},
                    {data: 'description'},
                    {data:null,render:function(data,type,row,meta){
                        return row.classroom_name;
                    }},
                    {data: 'status'},
                    {data: 'created_at',render:function(data,type,row,meta){
                        return new Date(data).toLocaleString('es-PY', { day: '2-digit', month: '2-digit', year: 'numeric' });
                    }},
                    {data: 'updated_at',render:function(data,type,row,meta){
                        return new Date(data).toLocaleString('es-PY', { day: '2-digit', month: '2-digit', year: 'numeric' });
                    }},
                    {data: 'priority'},
                    {data: null, render:function(data,type,row){
                        var btns = '<button data-action="edit" data-id="'+ row.id +'" class="btn btn-primary btn-sm">Editar</button>'
                        btns +='<button data-action="delete" data-id="'+ row.id +'" class="btn btn-danger btn-sm">Eliminar</button>'
                        btns += '<button data-action="show" data-id="'+ row.id +'" class="btn btn-primary btn-sm">Ver</button>'

                        return btns;
                    }}
                ],
                dom:
                `<'clearfix'
                >`
                +
                `<'row'
                    <'col-sm-12'tr>
                >`
                +
                `<'row'
                    <'col-sm-12 col-md-5'i>
                    <'col-sm-12 col-md-7'p>
                    
                >`
            });
        });
        $(document).on("click", "#ticketTable tr td button", function(event) {
            var action = $(this).data("action");
            var id = $(this).data("id");
            var method;
            var route;
            switch (action) {
                case "edit":
                    method = "get";
                    route = "{{route('proffessor.ticket.edit','xx')}}"
                    break;
                case "delete":
                    method = "delete";
                    route = "{{route('proffessor.ticket.destroy','xx')}}"
                    break;
                case "show":
                    method = "GET";
                    route = "{{route('proffessor.ticket.show','xx')}}";
                    break;
            }
            route = route.replace('xx',id);
            if(
                action == "delete"
            ){
                if(!confirm("¿Estas seguro de eliminar el ticket?")){
                    return false;
                }else{
                    $.ajax({
                        url:route,
                        type:method,
                        data:{
                            _token:'{{csrf_token()}}'
                        },
                        success:function(data){
                            var msg;
                            if(data.code){
                                $('#ticketTable').DataTable().ajax.reload();
                                msg = "El ticket se ha eliminado correctamente"
                            }else
                            {
                                msg = "El ticket no se ha podido eliminar"
                            }
                            alert(msg);
                        }
                    });

                }      
            }else{
                //redirect to edit
                window.location.href = route;
            }
        });
    </script>
@endsection