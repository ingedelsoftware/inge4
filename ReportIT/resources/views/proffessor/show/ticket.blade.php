@extends('config.default-style-doc')

@section('title', 'Creacion de ')

@section('content')
<div class="card">
    @if(session()->has('msg'))
    <div class="alert alert-success">{{session('msg')}}</div>
    @endif
    <div class="card-header">
        <h1>Creacion de Tickets</h1>
    </div> 
    <div class="card-body">
        
        @include('config.create-inputs',[
            'inputs'=>[
                [
                    'type'=>'text',
                    'name'=>'title',
                    'label'=>'Titulo',
                    'placeholder'=>'Titulo',
                    //if record empty then ''  else  $record->title
                    'value'=> !empty($record->id) ? $record->title : '',
                    'required'=>true,
                    'disabled'=>false,
                    'readonly'=>false,
                    'class'=>'form-control',
                    'id'=>'title',
                ],
                [
                    'type'=>'text',
                    'name'=>'description',
                    'label'=>'Descripcion',
                    'placeholder'=>'Descripcion',
                    'value'=> !empty($record->id) ? $record->description : '',
                    'required'=>true,
                    'disabled'=>false,
                    'readonly'=>false,
                    'class'=>'form-control',
                    'id'=>'description',
                ],
                [
                    'type'=>'select',
                    'name'=>'classroom_id',
                    'label'=>'Aula',
                    'placeholder'=>'Aula',
                    'value'=> !empty($record->id) ? $record->classroom_id : '',
                    'required'=>true,
                    'disabled'=>false,
                    'readonly'=>false,
                    'class'=>'form-control',
                    'options'=>[$record->classroom_id=>$record->classroom],
                    'options_contains_array'=>true,
                    'default'=>$record->classroom_id,
                    'id'=>'classroom_id',
                
                ],
                [
                    'type'=>'select',
                    'name'=>'status',
                    'label'=>'Estado',
                    'placeholder'=>'Estado',
                    'value'=> !empty($record->id) ? $record->status : '',
                    'required'=>true,
                    'disabled'=>false,
                    'readonly'=>false,
                    'class'=>'form-control',
                    'options'=>[
                        'abierto'=>'Abierto',
                        'cerrado'=>'Cerrado',
                        'pendiente'=>'Pendiente'
                    ],
                    'default'=>'abierto',
                    'id'=>'status',
                ],
                [
                    'type'=>'select',
                    'name'=>'priority',
                    'label'=>'Prioridad',
                    'placeholder'=>'Prioridad',
                    'value'=> !empty($record->id) ? $record->priority : '',
                    'required'=>true,
                    'disabled'=>false,
                    'readonly'=>false,
                    'class'=>'form-control',
                    'options'=>[
                        'baja'=>'Baja',
                        'media'=>'Media',
                        'alta'=>'Alta'
                    ],
                    'default'=>'',
                    'id'=>'priority',
                ]
            ],
            'method'=>'POST',
            'action'=>route('admin.ticket.solve',$record->id),
            'button'=>'Guardar'
        ])

    </div>
    <div >
        @csrf
        <div id="solutionSteps">
            Solucion del ticket
            @foreach($solutions as $solution)
            <div class="form-group">
                <input type="text" name="solution[]" id="solution" class="form-control" value="{{$solution->description}}">
            </div>
            @endforeach    
        </div>
        <button type="submit" class="btn btn-primary mt-2">Guardar</button>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('#addStep').on('click',function(){
        $('')
        var html = `
        <div class="form-group">
            <input type="text" name="solution[]" id="solution" class="form-control">
        </div>
        `;
        $('#solutionSteps').append(html);
    });
</script>
@endsection
