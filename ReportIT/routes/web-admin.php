<?php


use App\Http\Controllers\ClassroomController;
use App\Http\Controllers\ProffessorController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\StaffLoginController;
use App\Http\Controllers\StaffTicketController;
use App\Http\Controllers\TechnologyController;
use App\Models\Staff;
use Illuminate\Support\Facades\Route;
Route::prefix('admin')->group( function(){

    Route::middleware(['auth:admin','web'])->group(function () {
        // Your authenticated routes here
        Route::get('/',function(){
            
            return view('config.home');
        })->name('admin.home');
        Route::post('classroom/index/datatable',[ClassroomController::class,'list'])->name('admin.classroom.datatable');
        Route::resource('classroom',ClassroomController::class)->
        names([
            'index' => 'admin.classroom.index',
            'create' => 'admin.classroom.create',
            'store' => 'admin.classroom.store',
            'edit' => 'admin.classroom.edit',
            'update' => 'admin.classroom.update',
            'destroy' => 'admin.classroom.destroy',
        ]);
        Route::post('staff/index/datatable',[StaffController::class,'index'])->name('admin.staff.datatable');
        Route::resource('staff',StaffController::class)->
        names([
              'index' => 'admin.staff.index',
            'create' => 'admin.staff.create',
            'store' => 'admin.staff.store',
            'show' => 'admin.staff.show',
            'edit' => 'admin.staff.edit',
            'update' => 'admin.staff.update',
            'destroy' => 'admin.staff.destroy',
        ]);
        Route::resource('proffessor',ProffessorController::class)->
        names([
            'index' => 'admin.proffessor.index',
            'create' => 'admin.proffessor.create',
            'store' => 'admin.proffessor.store',
            'edit' => 'admin.proffessor.edit',
            'update' => 'admin.proffessor.update',
            'destroy' => 'admin.proffessor.destroy',
        ]);
        
        Route::post('technology/index/datatable',[TechnologyController::class,'list'])->name('admin.technology.datatable');
        Route::resource('technology',TechnologyController::class)->
        names([
            'index' => 'admin.technology.index',
            'create' => 'admin.technology.create',
            'store' => 'admin.technology.store',
            'edit' => 'admin.technology.edit',
            'update' => 'admin.technology.update',
            'destroy' => 'admin.technology.destroy',
        ]);
        
        Route::post('ticket/index/datatable',[StaffTicketController::class,'list'])->name('admin.ticket.datatable');
        Route::post('ticket/solve/{ticket}',[StaffTicketController::class,'solution'])->name('admin.ticket.solve');
        Route::resource('ticket',StaffTicketController::class)
        ->names(
            [
                'index' => 'admin.ticket.index',
                'show' => 'admin.ticket.show',
                'list' => 'admin.ticket.list',
                'solution'=>'admin.ticket.solution'
            ]
        );

        
    });


});