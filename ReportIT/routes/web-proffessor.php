<?php

use App\Http\Controllers\ProffessorTicketController;
use App\Http\Controllers\ProffessorLoginController;
use Illuminate\Support\Facades\Route;
Route::prefix('proffessor')->group( function(){

    
    Route::middleware(['auth:proffessor','web'])->group(function () {
        Route::get('/',function(){
    
            return view('config.home');
        })->name('proffessor.home');
        
        // Your authenticated routes here
    
    Route::post('ticket/index/datatable',[ProffessorTicketController::class,'list'])->name('proffessor.ticket.datatable');
    Route::resource('ticket',ProffessorTicketController::class)
    ->names([
        'index'=>'proffessor.ticket.index',
        'create'=>'proffessor.ticket.create',
        'store'=>'proffessor.ticket.store',
        'show'=>'proffessor.ticket.show',
        'edit'=>'proffessor.ticket.edit',
        'update'=>'proffessor.ticket.update',
        'destroy'=>'proffessor.ticket.destroy',
    ]);
    
    });
});