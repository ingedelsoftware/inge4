<?php

use App\Http\Controllers\ProffessorLoginController;
use App\Http\Controllers\StaffLoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/',function(){
   return view('welcome');
})->name('welcome');
Route::middleware('web')->group(function(){
    Route::get('admin/login',[StaffLoginController::class,"loginView"])->name('admin.login');
    Route::get('proffessor/login',[ProffessorLoginController::class,"loginView"])->name('proffessor.login');
    
    
    Route::post('admin/login',[StaffLoginController::class,"login"]);
    Route::post('proffessor/login',[ProffessorLoginController::class,"login"]);
    
    Route::get('admin/logout',[ProffessorLoginController::class,"logout"])->name('proffessor.logout');
    Route::get('proffessor/logout',[StaffLoginController::class,"logout"])->name('admin.logout');
});
