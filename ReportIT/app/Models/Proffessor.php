<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
class Proffessor extends Model implements AuthenticatableContract
{
    use HasFactory;
    public function getAuthIdentifierName()
    {
        return 'id'; // Replace with your primary key column name
    }

    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    public function getAuthPassword()
    {
        return $this->password;
    }
    public function getRememberTokenName()
    {
        return null; // not supported
    }
    public function getRememberToken()
    {
        return null; // not supported
    }
    public function setRememberToken($value)
    {
        // not supported
    }
    protected $table = 'proffessors';
    protected $fillable =
    [
        'username',
        'password',
        'people_id'
    ];
    public function people(){
        return $this->belongsTo('App\Models\People\People');
    }

}
