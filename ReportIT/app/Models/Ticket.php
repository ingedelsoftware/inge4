<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;
    protected $table = 'tickets';
    
    protected $fillable = [
        'proffessor_id',
        'title',
        'description',
        'classroom_id',
        'status'
    ];
    
    public function proffessor(){
        return $this->belongsTo('App\Models\Proffessor');
    }
    public function classroom(){
        return $this->belongsTo('App\Models\Classroom');
    }
    
}
