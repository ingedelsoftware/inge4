<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    use HasFactory;
    protected $table = 'classroom';
    protected $fillable = [
        'name'
    ];
    public function classroomHasTechnology(){
        return $this->hasMany('App\Models\ClassroomHasTechnology');
    }
}
