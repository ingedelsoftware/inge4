<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    use HasFactory;
    protected $table = 'people';
    protected $fillable =
    [
        'name',
        'identification'
    ];
    public function proffessor(){
        return $this->hasOne('App\Models\Proffessor');
    }

    public function staff(){
        return $this->hasOne('App\Models\Staff');
    }
}
