<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreStaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->guard('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {

        return [
            'username' => ['required','string', 'max:255'],
            'password' => ['required','string', 'max:255'],
            'name' => ['required','string','max:255'],
            'lastname' => ['required','string','max:255'],
            'email' => ['required','string','max:255'],
            'phone' => ['nullable','string','max:255'],
            'address' => ['nullable','string','max:255'],

        ];
    }
}
