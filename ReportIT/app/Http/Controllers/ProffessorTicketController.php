<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTicketRequest;
use App\Http\Requests\UpdateTicketRequest;
use App\Models\Classroom;
use App\Models\Ticket;
use App\Models\TicketDetail;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProffessorTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('proffessor.index.ticket');
    }
    public function list(request $request){
        $query = Ticket::where('proffessor_id',Auth::guard('proffessor')->user()->id)->join('classroom','classroom.id','=','tickets.classroom_id');
        $columns = [
            'tickets.id',
            'tickets.title',
            'tickets.description',
            DB::raw('classroom.name as classroom_name'),
            'tickets.priority',
            'tickets.status',
            'tickets.created_at',
            'tickets.updated_at'
        ];
        $final_query = $query->select($columns)
        ->limit($request->length)
        ->orderBy(DB::raw($columns[$request->order[0]['column']]),$request->order[0]['dir'])
        ->get();



        return response()->json([
            'success' => true,
            'recordsTotal'=>Ticket::where('proffessor_id',Auth::guard('proffessor')->user()->id)->count(),
            'recordsFiltered'=>$final_query->count(),
            'data'=>$final_query,
            'params'=>$_GET,
            'draw'=>(int)$request->draw

        ]);
        
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $classrooms =
         Classroom::all();
        return view('proffessor.forms.ticket')->with('classrooms',$classrooms);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreTicketRequest $request)
    {
        try{
            DB::beginTransaction();
            //se crea la cabecera
            $ticket = new Ticket();
            $ticket->title = $request->title;
            $ticket->description = $request->description;
            $ticket->classroom_id = $request->classroom_id;
            $ticket->status = $request->status;
            $ticket->priority = $request->priority;
            $ticket->proffessor_id = Auth::guard('proffessor')->user()->id;
            $ticket->save();
            //se crea el detalle
            // foreach($request->classroomTech as $tech){
            //     $technology[] = [
            //         'id' => $tech->tech_id,
            //         'description' => $tech
            //     ];
            //     $json = json_encode($technology);
            //     $ticketDetail = new TicketDetail();
            //     $ticketDetail->description = $json;
            //     $ticketDetail->ticket_id = $ticket->id;
            //     $ticketDetail->save();
            // }
            DB::commit();
        }catch (Exception $e){
            DB::rollBack();
            return back()->with('error','Error al crear el registro');
        }
        return redirect()->route('proffessor.ticket.index');

    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Ticket $ticket)
    {
        
        if($ticket->status == 'cerrado'| $ticket->proffessor_id != Auth::guard('proffessor')->user()->id){
            return back()->with('error','Solo se pueden editar tickets abiertos y creados por el mismo profesor');
        }else{
            $classrooms =
            Classroom::all();
   
            return view('proffessor.forms.ticket')->with('record',$ticket)->with('classrooms',$classrooms);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTicketRequest $request, Ticket $ticket)
    {
        try{
            DB::beginTransaction();
            //se crea la cabecera
            $ticket->title = $request->title;
            $ticket->description = $request->description;
            $ticket->classroom_id = $request->classroom_id;
            $ticket->status = $request->status;
            $ticket->proffessor_id = Auth::guard('proffessor')->user()->id;
            $ticket->save();
            //se crea el detalle
            foreach($request->classroomTech as $tech){
                $technology[] = [
                    'id' => $tech->tech_id,
                    'description' => $tech
                ];
                $json = json_encode($technology);
                if(TicketDetail::find($tech->detail_id)){
                    $ticketDetail = TicketDetail::find($tech->detail_id);
                }else{
                    $ticketDetail = new TicketDetail();
                    $ticketDetail->ticket_id = $ticket->id;

                }
                $ticketDetail->description = $json;
                $ticketDetail->save();
            }
            DB::commit();
        }catch (Exception $e){
            DB::rollBack();
            return back()->with('error','Error al actualizar el registro');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Ticket $ticket)
    {
        if ($ticket->status == 'cerrado'){
            return back()->with('error','Solo se pueden eliminar tickets abiertos');
        }else{
            $code = $ticket->delete();
            
            return response()->json(['success'=>true,'code'=>$code]);
        }
    }
    public function show(Ticket $ticket){
        $solutions = TicketDetail::where('ticket_id',$ticket->id)->get();
        return view('proffessor.show.ticket')->with('record',$ticket)->with('solutions',$solutions);
    }
}
