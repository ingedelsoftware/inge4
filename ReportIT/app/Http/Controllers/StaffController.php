<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StorePeopleRequest;
use App\Http\Requests\StoreStaffRequest;
use App\Http\Requests\UpdatePeopleRequest;

use App\Models\People;
use App\Models\Staff;
class StaffController extends Controller
{
    public function index(Request $request){
        if ($request->expectsJson()){
            $columnNames = [
                'id',
                'username' ,
                'name' ,
                'lastname' ,
                'email' ,
                'phone' ,
                'address'
            ];
            $query = Staff::select($columnNames)->get();
            
            return response()->json([
                'success' => true,
                'recordsTotal'=>Staff::count(),//en este caso 
                'recordsFiltered'=>Staff::count(),
                'data'=>$query,
                'params'=>$_GET,
                'draw'=>(int)$request->draw
            ]);
        }else{
            return view('staff.index.staff');

            //rute post a staff.index
        }
    }
    public function create(){
        return view('staff.forms.staff');
        //inputs: username, password, name, lastname, email ,phone
    }
    public function store(StoreStaffRequest $request){
        
        $staff = new staff();
        $staff->username = $request->username;
        $staff->password = bcrypt($request->password);//hashear contrasenha
        $staff->name = $request->name;
        $staff->lastname = $request->lastname;
        $staff->email = $request->email;
        $staff->phone = $request->phone;
        $staff->address = $request->address;
        $code = $staff->save();
        if($code){
            return redirect()->route('admin.staff.index');
        }else{
            return back()->with('error','Error al crear el registro');
        }
    }

    public function edit(Staff $staff){
        return view('staff.form',compact('staff'));
        //inputs: username, password 
        //inputs people.id, people.name,people.identification no editable
    }
    public function update(UpdatePeopleRequest $request,Staff $staff){
        if ($request->password != null){
            $staff->password = ($request->password);//hashear contrasenha
        }
        
        $staff->name = $request->name;
        $staff->lastname = $request->lastname;
        $staff->email = $request->email;
        $staff->phone = $request->phone;
        $staff->address = $request->address;
        $code = $staff->save();
        
        if($code){
            return redirect()->route('staff.index');
        }else{
            return back()->with('error','Error al actualizar el registro');
        }
    }

    public function destroy(Staff $staff){
        $code = $staff->delete();
        if($code){
            return redirect()->route('staff.index');
        }else{
            return back()->with('error','Error al eliminar el registro');
        }
    }
}
