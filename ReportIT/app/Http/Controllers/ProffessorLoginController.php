<?php

namespace App\Http\Controllers;

use App\Models\Proffessor;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProffessorLoginController extends Controller
{
    public function loginView(){
        if(auth()->guard('proffessor')->check()){
            return redirect()->route('proffessor.home');
        }
        return view('proffessor.login');
    }
    public function login(Request $request){
        $username = $request->username;
        $password = $request->password;
        
        $user  =User::where('username',$username)->first();

        

        if(!$user){
            return back()->withInput()->with('msg','Usuario o contraseña incorrectos');
        }
        if(password_verify($password,$user->password)){
            auth()->guard('proffessor')->login($user);
            return redirect()->route('admin.home');
        }else{
           return back()->withInput()->with('msg','Usuario o contraseña incorrectos');
        }
    }
    public function logout(){
        Auth::guard('proffessor')->logout();
        return redirect()->route('welcome');
    }
}
