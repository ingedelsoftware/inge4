<?php

namespace App\Http\Controllers;

use App\Models\Classroom;
use App\Models\Technology;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TechnologyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('staff.index.technology');
    }
    public function list(request $request){
        $query = Technology::query();
        $columns = [
            'technologies.id',
            'technologies.type',
            'technologies.specs',
            DB::Raw('staff.name as name'),
            DB::Raw('staff.lastname as last_name'),
           
        ];
        $final_query = $query->select($columns)
        ->limit($request->length)
        ->offset($request->start)
        ->join('staff','staff.id','=','technologies.staff_id')
        ->orderBy(DB::raw($columns[$request->order[0]['column']]),$request->order[0]['dir'])
        ->get();
        return response()->json([
            'success' => true,
            'recordsTotal'=>Technology::count(),
            'recordsFiltered'=>  Technology::select($columns)
            ->join('staff','staff.id','=','technologies.staff_id')
            ->orderBy(DB::raw($columns[$request->order[0]['column']]),$request->order[0]['dir'])
            ->get()->count(),
            'data'=>$final_query,
            'params'=>$_GET,
            'draw'=>(int)$request->draw

        ]); 
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $classrooms = Classroom::all();
        return view('staff.forms.technology',compact('classrooms'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $record = new Technology();
        $specs = [];
        if($request->type == 'cpu'){
            $specs['cpu'] = $request->cpu;
            $specs['mb'] = $request->mb;
            foreach($request->other as $key => $value){
                $specs['other'][] = $value;
            }
        }
        else{
            $specs['brand'] = $request->brand;
            $specs['model'] = $request->model;
        }
        $record->type = $request->type;
        $record->specs = json_encode($specs);
        $record->staff_id = auth()->guard('admin')->user()->id;
        $code = $record->save();
        return response()->json([
            'success' => $code,
            'url' => route('admin.technology.index')
            
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Technology $technology)
    {
        $classrooms = Classroom::all();
        return view('staff.forms.technology')->with('record',$technology)->with('classrooms',$classrooms);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Technology $technology)
    {
        $specs = [];
        if($request->type == 'cpu'){
            $specs['cpu'] = $request->cpu;
            $specs['mb'] = $request->mb;
            foreach($request->other as $key => $value){
                $specs['other'][] = $value;
            }
        }
        else{
            $specs['brand'] = $request->brand;
            $specs['model'] = $request->model;
        }
        $technology->type = $request->type;
        $technology->specs = json_encode($specs);
        $technology->staff_id = auth()->guard('admin')->user()->id;
        $code = $technology->save();
        return response()->json([
            'success' => $code,
            'url' => route('admin.technology.index')
            
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Technology $technology)
    {
        $code = $technology->delete();
        return response()->json([
            'success' => true,
            'code' => $code
        ]);
    }
}
