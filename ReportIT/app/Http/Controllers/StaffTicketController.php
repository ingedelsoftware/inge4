<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTicketDetailRequest;
use App\Models\Ticket;
use App\Models\TicketDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StaffTicketController extends Controller
{
    public function index()
    {
        
        return view('staff.index.ticket');
    }
    
    public function list(request $request){
        $columns = [
            'tickets.id',
            'title',
            'description',
            'proffessor_id',
            'classroom_id',
            'priority',
            'tickets.created_at',
            'tickets.updated_at',
            'status',
            'proffessors.name',
            'proffessors.lastname',
        ];
        $final_query = Ticket::select($columns)
        ->join('proffessors','proffessors.id','=','tickets.proffessor_id')
        ->limit($request->length)
        ->orderBy(DB::raw($columns[$request->order[0]['column']]),$request->order[0]['dir'])
        ->get();



        return response()->json([
            'success' => true,
            'recordsTotal'=>Ticket::count(),
            'recordsFiltered'=>$final_query->count(),
            'data'=>$final_query,
            'params'=>$_GET,
            'draw'=>(int)$request->draw

        ]);
        
    }

    public function show(Ticket $ticket){
        $solutions = TicketDetail::where('ticket_id',$ticket->id)->get();
        return view('staff.shows.ticket')->with('record',$ticket)->with('solutions',$solutions);
    }
    public function solution(Ticket $ticket,StoreTicketDetailRequest $request){
        $ticket->status = 'cerrado';
        $ticket->save();
        TicketDetail::where('ticket_id',$ticket->id)->delete();
        foreach ($request->solution as $step){
            if($step != null){
                $ticketDetail = new TicketDetail();
                $ticketDetail->ticket_id = $ticket->id;
                $ticketDetail->staff_id = auth()->user()->id;
                $ticketDetail->description = $step;
                $ticketDetail->save();
            }
        }
        return redirect()->route('admin.ticket.index')->with('success','Ticket solved');
    }
}
