<?php

namespace App\Http\Controllers;

use App\Models\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class StaffLoginController extends Controller
{
    public function loginView(){
        
        if(auth()->guard('admin')->check()){
            return redirect()->route('admin.home');
        }
        return view('staff.login');
    }
    public function login(Request $request){
        $username = $request->username;
        $password = $request->password;
        
        $user  = Staff::where('username',$username)->first();

        

        if(!$user){
            return back()->withInput()->with('msg','Usuario o contraseña incorrectos');
        }
        if(password_verify($password,$user->password)){
            auth()->guard('admin')->login($user);
            return redirect()->route('admin.home');
        }else{
           return back()->withInput()->with('msg','Usuario o contraseña incorrectos');
        }
    }
    public function logout(){
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }
}
