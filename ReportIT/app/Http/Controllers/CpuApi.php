<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CpuApi extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $file_name = 'cpulist.json';
        $cpuList = [];
        $json_string = file_get_contents(base_path($file_name));
        $rows = json_decode($json_string,true);
        
        $rows = $rows['data'];
        foreach($rows as $row){
            if($row['cat'] == 'Desktop' ){
                
                $cpuList[] = $row['name'];
            }
        }
        return response()->json([
            'success' => true,
            'data'=>$cpuList,
        ],200);
    }

   
}
