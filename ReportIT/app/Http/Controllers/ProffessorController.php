<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProffessorRequest;
use App\Http\Requests\UpdateProffessorRequest;
use App\Models\People;
use App\Models\Proffessor;
use Illuminate\Http\Request;

class ProffessorController extends Controller
{
    public function index(){
        return view('proffessor.index');
    }
    public function create(){
        return view('proffessor.form');
        //inputs: username, password 
        //inputs: people.id, people.name,people.identification  
        //esto deberia tener un autocomplete que deje escribir nombre y identificacion
        //si el boton de nueva persona esta activo debe intentar crear una nueva persona
    }
    public function store(StoreProffessorRequest $request){
        if($request->people_id == null){
            $people = new People();
            $people->identification = $request->identification;
            $people->name = $request->name;
            $people->save();
            $people_id = $people->id;
        }else{
            $people_id = $request->people_id;
        }
        $proffessor = new Proffessor();
        $proffessor->username = $request->username;
        $proffessor->password = ($request->password);//hashear contrasenha
        $proffessor->people_id = $people_id;
        $code = $proffessor->save();
        if($code){
            return redirect()->route('proffessor.index');
        }else{
            return back()->with('error','Error al crear el registro');
        }
    }

    public function edit(Proffessor $proffessor){
        return view('proffessor.form',compact('proffessor'));
        //inputs: username, password 
        //inputs people.id, people.name,people.identification no editable
    }
    public function update(UpdateProffessorRequest $request,Proffessor $proffessor){
        $proffessor->username = $request->username;
        $proffessor->password = ($request->password);//hashear contrasenha
        $code = $proffessor->save();
        if($code){
            return redirect()->route('proffessor.index');
        }else{
            return back()->with('error','Error al actualizar el registro');
        }
    }

    public function destroy(Proffessor $proffessor){
        $code = $proffessor->delete();
        if($code){
            return redirect()->route('proffessor.index');
        }else{
            return back()->with('error','Error al eliminar el registro');
        }
    }
}
