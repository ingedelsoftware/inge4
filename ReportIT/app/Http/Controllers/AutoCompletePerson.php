<?php

namespace App\Http\Controllers;

use App\Models\People;
use Illuminate\Http\Request;

class AutoCompletePerson extends Controller
{
    public function autoComplete(Request $request){
        $search = isset($request->identification) ? $request->people_id : $request->name;
        if(isset($request->identification) ){
            $data = People::select("id","name")
            ->where('identification','LIKE',"%$search%")
            ->get();
        }else{
            $data = People::select("id","name")
            ->where('name','LIKE',"%$search%")
            ->get();
        }
        return response()->json($data);
    }
}
