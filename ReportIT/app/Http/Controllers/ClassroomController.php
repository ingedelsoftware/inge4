<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreClassroomRequest;
use App\Http\Requests\UpdateClassroomRequest;
use App\Models\Classroom;
use Exception;
use Illuminate\Support\Facades\DB;

class ClassroomController extends Controller
{
    public function index(){
        return view('staff.index.classroom');
    }
    public function create(){
        return view('staff.forms.classroom');
        //inputs: name
    }
    public function list(request $request){
        $query = Classroom::query();
        $columns = [
            'id',
            'name',
            'created_at',
            'updated_at'
        ];
        $final_query = $query->select($columns)
        ->limit($request->length)
        ->orderBy(DB::raw($columns[$request->order[0]['column']]),$request->order[0]['dir'])
        ->get();
        return response()->json([
            'success' => true,
            'recordsTotal'=>Classroom::count(),
            'recordsFiltered'=>$final_query->count(),
            'data'=>$final_query,
            'params'=>$_GET,
            'draw'=>(int)$request->draw

        ]);
    }
    public function store(StoreClassroomRequest $request){
        $classroom = new Classroom();
        $classroom->name = $request->name;
        $classroom->staff_id = auth()->guard('admin')->user()->id;
        $code = $classroom->save();
        if($code){
            return redirect()->route('admin.classroom.index');
        }else{
            return back()->with('error','Error al crear el registro');
        }
    }
    public function edit(Classroom $classroom){
        return view('classroom.form',compact('classroom'));
        //inputs: name
    }
    public function update(UpdateClassroomRequest $request,Classroom $classroom){
        
        $classroom->name = $request->name;
        $code = $classroom->save();
        if($code){
            return redirect()->route('classroom.index');
        }else{
            return back()->with('error','Error al actualizar el registro');
        }
    }
    public function destroy(Classroom $classroom){
        try{

            $code = $classroom->delete();

        }catch(Exception $e){
            return response()->json([

            'error'=>'Error al eliminar el registro']);
        }
        return response()->json([
            'success' => true,
            'code' => $code
        ]);
    }
    public function show(){
        dd();
    }
}
