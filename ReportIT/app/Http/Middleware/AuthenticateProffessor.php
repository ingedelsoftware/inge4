<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthenticateProffessor
{
    public function handle(Request $request, Closure $next)
    {
        
        if (!Auth::guard('proffessor')->check()) {
            return redirect()->route('proffessor.login');
        }

        return $next($request);
    }
}
