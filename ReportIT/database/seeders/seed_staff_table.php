<?php

namespace Database\Seeders;

use App\Models\Staff;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class seed_staff_table extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Staff::create([
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'name' => 'admin',
            'lastname' => 'admin',
            'email' => 'admin@gmail.com'
        ]);
    }
}
