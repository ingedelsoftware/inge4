<?php

namespace Database\Seeders;

use App\Models\Proffessor;
use App\Models\Staff;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class seed_user_proffessor_table extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $staff = Staff::where('email','admin@gmail.com')->first();
        
        Proffessor::create([
            'name' => 'proffessor1',
            'lastname' => 'proffessor1',
            'staff_id'=> $staff->id,
            'email' => 'proffessor1.@gmail.com'
        ]);
        $proffessor = Proffessor::where('email','proffessor1.@gmail.com')->first();
        User::create([
            'username' => 'proffessor1',
            'password' => bcrypt('proffessor1'),
            'proffessor_id' => $proffessor->id
        ]);
    }
}
