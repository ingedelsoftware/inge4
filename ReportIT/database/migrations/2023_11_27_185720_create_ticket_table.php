<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->integer('proffessor_id')->unsigned();
            $table->foreign('proffessor_id')->references('id')->on('proffessors');
            $table->string('title',50);
            $table->string('description',255);
            $table->integer('classroom_id')->unsigned();
            $table->foreign('classroom_id')->references('id')->on('classroom');
            $table->string('status',50);
            $table->string('priority',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    { 
        Schema::dropIfExists('ticket');
    }
};
